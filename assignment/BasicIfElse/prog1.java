/*

   WRP to check if a number is even or odd.

*/

class Check{

	public static void main(String[] args){

		int num = 10;

		if(num % 2 == 0)
			System.out.println(num + " is even number");
		else
			System.out.println(num + " is odd number");

	}
}
