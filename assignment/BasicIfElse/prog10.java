/*

   Write a realtime example of if else if ladder

*/

class RltExample{

	public static void main(String[] args){

		float percent = 67.20f;

		if(percent < 35f)
			System.out.println("Fail");

		else if(percent < 60f)
			System.out.println("Class C");

		else if(percent < 75f)
			System.out.println("Class B");

		else 
			System.out.println("Class A");
	}
}
