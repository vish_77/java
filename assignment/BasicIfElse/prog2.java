/*

   WRP take a number, and print whether it is less than 10 or greater than 10.

*/

class Check{

	public static void main(String[] args){

		int num = 20;

		if(num > 10)
			System.out.println(num + " is greater than 10");
		else if(num < 10)
			System.out.println(num + " is less than 10");
		else
			System.out.println(num + " is equal to 10");
	}
}
