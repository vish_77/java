/*

   WRP take a number and print whether it is positive or negative

*/

class Check{

	public static void main(String[] args){

		int num = 10;

		if(num == 0)
			System.out.println("0 is not positive nor negative");
		else if(num > 0)
			System.out.println(num+" is positive");
		else
			System.out.println(num+" is negative");
	}
}
