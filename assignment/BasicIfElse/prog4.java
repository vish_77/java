/*

   WRP that checks a number from 0 to 5 and prints its spelling, if the number is greater
   than 5 print the number is greater than 5

*/

class Spellings{

	public static void main(String[] args){

		int num = 5;

		if(num > 5)
			System.out.println("Number is greater than 5");
		else if(num < 0)
			System.out.println("Number is less than 0");
		else if(num == 0)
			System.out.println("Zero");
		
		else if(num == 1)
                        System.out.println("One");

		else if(num == 2)
                        System.out.println("Two");

		else if(num == 3)
                        System.out.println("Three");

		else if(num == 4)
                        System.out.println("Four");

		else if(num == 5)
                        System.out.println("Five");
	}
}

