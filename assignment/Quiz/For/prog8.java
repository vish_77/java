class Core2Web{

	public static void main(String[] args){

		System.out.println("Before for loop");

		for(int i,j; i < 3; i++){

			System.out.println("Inside for");	//error: variable i might not have been initialized
		}
		System.out.println("After for loop");
	}
}
