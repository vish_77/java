class Core2Web{

	public static void main(String[] args){

		System.out.println("Before for loop");

		//error: variable i might not have been initialized
		//error: variable j might not have been initialized
		for(int i,j; i < 3; i++,j++){

			System.out.println("Inside for");
		}
		System.out.println("After for loop");

	}
}
