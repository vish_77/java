class Core2Web{

	public static void main(String[] args){

		int arr[5] = {1,2,3,4,5};	//size and initializer list cannot be given at the same time to array.

		for(int var : arr)
			System.out.println(var);
	}
}
