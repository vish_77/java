/*
1] Search an Element in an array

Given an integer array and another integer element. The task is to find if the given
element is present in the array or not.
Example 1:
Input:
n = 4
arr[] = {1,2,3,4}
x = 3
Output: 2
Explanation: There is one test case with an array as {1, 2, 3 4} and an
element to be searched as 3. Since 3 is present at index 2, output is 2.
*/


import java.io.*;

class Client{

	static int  flag = 0;

	static int Search(int arr[],int x){

		for(int i = 0; i<arr.length; i++){

			if(arr[i] == x){
				flag = 1;
				return i;
			}
		}

		return -1;
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
	
		System.out.println("Enter an element to search = ");
		int x = Integer.parseInt(br.readLine());

		int index = Search(arr,x);

		if(flag == 1)
			System.out.println("Element "+x+" found at index "+index);
		else
			System.out.println("Element "+x+" not present in array");
	
	}
}
