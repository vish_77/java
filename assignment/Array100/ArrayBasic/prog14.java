/*
14] Maximum repeating number

Given an array Arr of size N, the array contains numbers in range from 0 to K-1
where K is a positive integer and K <= N. Find the maximum repeating number in
this array. If there are two or more maximum repeating numbers return the element
having least value.
Example 1:
Input:
N = 4, K = 3
Arr[] = {2, 2, 1, 2}
Output: 2
Explanation: 2 is the most frequent element.
Example 2:
Input:
N = 6, K = 3
Arr[] = {2, 2, 1, 0, 0, 1}
Output: 0
Explanation: 0, 1 and 2 all have the same frequency of 2.But since 0 is
smallest, you need to return 0.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(K)
Constraints:
1 <= N <= 10^7
1 <= K <= N
0 <= Arri <= K - 1

*/


import java.io.*;

class Client{

	static void MaxRepeating(int arr[]){

		int max = -2147483648;

		for(int i = 0; i<arr.length; i++){

			if(arr[i] > max)
				max = arr[i];
		}
			
		int arr1[] = new int[max+1];
		
		for(int i = 0; i<arr.length; i++){

			arr1[arr[i]] += 1;
		}

		int max1 = arr1[0];

		for(int i = 0 ;i<arr1.length; i++){
			
			if(arr1[i] > max1){
				max1 = arr1[i];
			}
		}
		
		int x = 0;
		int ele = -1;
		for(int i = 0; i<arr1.length; i++){

			if(max1 == arr1[i]){
				ele = i;
				break;
			}
				
		}

		System.out.println("Maximum repeating element : "+ele);

	}



	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
		MaxRepeating(arr);	
	}
}
