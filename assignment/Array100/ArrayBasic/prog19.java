/*
19] Find common elements in three sorted arrays

Given three Sorted arrays in non-decreasing order, print all common elements in
these arrays.
Examples:
Input:
ar1[] = {1, 5, 10, 20, 40, 80}
ar2[] = {6, 7, 20, 80, 100}
ar3[] = {3, 4, 15, 20, 30, 70, 80, 120}
Output: 20, 80
Input:
ar1[] = {1, 5, 5}
ar2[] = {3, 4, 5, 5, 10}
ar3[] = {5, 5, 10, 20}
Output: 5, 5

*/


import java.io.*;

class Client{

	static void CommonEle(int arr1[], int arr2[], int arr3[]){

		int itr1 = 0;
		int itr2 = 0;
		int itr3 = 0;

		while(itr1 < arr1.length && itr2 < arr2.length && itr3 < arr3.length){

			
			if(arr1[itr1] == arr2[itr2] && arr1[itr1] == arr3[itr3])
				System.out.println("ele :"+arr1[itr1]);
			
			if(arr1[itr1] <= arr2[itr2] && arr1[itr1] <= arr3[itr3])
				itr1++;

			else if(arr2[itr2] <= arr1[itr1] && arr2[itr2] <= arr3[itr3])
				itr2++;

			else
				itr3++;
		}
		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array1 size = ");
		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size1; i++){

			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array2 size = ");
		int size2 = Integer.parseInt(br.readLine());

		int arr2[] = new int[size2];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size2; i++){

			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array3 size = ");
		int size3 = Integer.parseInt(br.readLine());

		int arr3[] = new int[size3];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size3; i++){

			arr3[i] = Integer.parseInt(br.readLine());
		}
	
		CommonEle(arr1,arr2,arr3);	
	}
}
