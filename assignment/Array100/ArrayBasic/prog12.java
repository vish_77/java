/*
12] First and last occurrences of X
Given a sorted array having N elements, find the indices of the first and last
occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.
Example 1:
Input:
N = 4 , X = 3
arr[] = { 1, 3, 3, 4 }
Output:
1 2
Explanation:
For the above array, first occurance of X = 3 is at index = 1 and last
occurrence is at index = 2.

Example 2:
Input:
N = 4, X = 5
arr[] = { 1, 2, 3, 4 }
Output:
-1
Explanation:
As 5 is not present in the array, so the answer is -1.
Expected Time Complexity: O(log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10^5

0 <= arr[i], X <= 10^9

*/


import java.io.*;

class Client{

	static void Occurance(int arr[],int element){

		int firstOcc = -1;
		int lastOcc = -1;

		for(int i = 0; i<arr.length; i++){

			if(arr[i]  == element)
				lastOcc = i;
		}
		
		for(int i = 0; i<arr.length; i++){

			if(arr[i]  == element){
				firstOcc = i;
				break;
			}
		}

		if(firstOcc == -1)
			System.out.println(-1);		
		else{
			System.out.println("First occurance = "+firstOcc);		
			System.out.println("Last occurance = "+lastOcc);		
		}
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter Element = ");
		int element = Integer.parseInt(br.readLine());

	
		Occurance(arr,element);	
	}
}
