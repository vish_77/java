/*
28] Remove Duplicates from unsorted array

Given an array of integers which may or may not contain duplicate elements. Your
task is to remove duplicate elements, if present.
Example 1:
Input:
N = 6
A[] = {1, 2, 3, 1, 4, 2}
Output:
1 2 3 4
Example 2:
Input:
N = 4
A[] = {1, 2, 3, 4}
Output:
1 2 3 4
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5

*/


import java.io.*;

class Client{

	static void MinMax(int arr[]){

		if(arr.length == 1){

			System.out.println("Min = "+arr[0] +" Max = "+arr[0]);		
			return;
		}

		int min = arr[0];
		int max = -2147483648;

		for(int i = 0; i<arr.length; i++){

			if(arr[i] > max)
				max = arr[i];
			
			if(arr[i] < min)
				min = arr[i];
		}
			
		System.out.println("Min = "+min +" Max = "+max);		
		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
		MinMax(arr);	
	}
}
