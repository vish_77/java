/*
13] Find unique element

Given an array of size n which contains all elements occurring in multiples of K,
except one element which doesn't occur in multiple of K. Find that unique element.
Example 1:
Input :
n = 7, k = 3
arr[] = {6, 2, 5, 2, 2, 6, 6}
Output :
5
Explanation:
Every element appears 3 times except 5.
Example 2:
Input :
n = 5, k = 4
arr[] = {2, 2, 2, 10, 2}
Output :
10
Explanation:
Every element appears 4 times except 10.
Expected Time Complexity: O(N. Log(A[i]) )
Expected Auxiliary Space: O( Log(A[i]) )
Constraints:
3<= N<=2*10^5
2<= K<=2*10^5
1<= A[i]<=10^9

*/


import java.io.*;

class Client{

	static void Unique(int arr[],int k){

		int arr1 [] = new int [1000];

		for(int i = 0; i<arr.length; i++){

			arr1[arr[i]] = arr1[arr[i]] + 1;
			
		}
			
		for(int i = 0; i<arr.length; i++){

			if(arr1[arr[i]] % k != 0)
				System.out.println("Odd element ="+arr[i]);				
			
		}
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter multiple = ");
		int k = Integer.parseInt(br.readLine());

		Unique(arr,k);	
	}
}
