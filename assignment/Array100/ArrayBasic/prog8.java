/*
8] Even occurring elements
Given an array Arr of N integers that contains an odd number of occurrences for all
numbers except for a few elements which are present even number of times. Find
the elements which have even occurrences in the array.
Example 1:
Input:
N = 11
Arr[] = {9, 12, 23, 10, 12, 12,
15, 23, 14, 12, 15}
Output: 12 15 23
Example 2:
Input:
N = 5
Arr[] = {23, 12, 56, 34, 32}
Output: -1
Explanation:
Every integer is present odd number of times.
*/


import java.io.*;

class Client{

	static void EvenElements(int arr[]){

		int arr1[] = new int[66];

		for(int i = 0; i<arr.length; i++){

			arr1[arr[i]] = arr1[arr[i]] + 1;
		}

		System.out.println("Evenly occured numbers");

		for(int i = 0; i<arr1.length; i++){

			if(arr1[i] != 0 && arr1[i] % 2 == 0){
				System.out.println(i);
			}		
		
		}
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
		EvenElements(arr);	
	}
}
