/*
11] Product of maximum in first array and minimum in second

Given two arrays of A and B respectively of sizes N1 and N2, the task is to
calculate the product of the maximum element of the first array and minimum
element of the second array.
Example 1:
Input : A[] = {5, 7, 9, 3, 6, 2},
B[] = {1, 2, 6, -1, 0, 9}
Output : -9
Explanation:
The first array is 5 7 9 3 6 2.
The max element among these elements is 9.
The second array is 1 2 6 -1 0 9.
The min element among these elements is -1.
The product of 9 and -1 is 9*-1=-9.
Example 2:
Input : A[] = {0, 0, 0, 0},
B[] = {1, -1, 2}
Output : 0
Expected Time Complexity: O(N + M).
Expected Auxiliary Space: O(1).
Output:
For each test case, output the product of the max element of the first array and the
minimum element of the second array.
Constraints:
1 ≤ N, M ≤ 10^6

-10^8 ≤ Ai, Bi ≤ 10^8

*/


import java.io.*;

class Client{

	static int Product(int arr1[], int arr2[]){

		int max = -2147483648;
		int min = arr2[0];

		for(int i = 0; i<arr1.length; i++){

			if(arr1[i] > max)
				max = arr1[i];
		}

		for(int i = 1; i<arr2.length; i++){

			if(arr2[i] < min)
				min = arr2[i];
		}
			
		return max*min;
		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array1 size = ");
		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size1; i++){

			arr1[i] = Integer.parseInt(br.readLine());
		}
	
		System.out.println("Enter array2 size = ");
		int size2 = Integer.parseInt(br.readLine());

		int arr2[] = new int[size2];

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size2; i++){

			arr2[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Product :"+Product(arr1,arr2));	
	}
}
