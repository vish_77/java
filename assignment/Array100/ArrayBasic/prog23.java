/*
23] Find the smallest and second smallest element in an array

Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.
Example 2:
Input :
6
1 2 1 3 6 7
Output :
1 2
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5

*/


import java.io.*;

class Client{

	static void Small(int arr[]){

		if(arr.length == 1){

			System.out.println("Smallest is = "+arr[0] +" Second smallest dosent exists");		
			return;
		}

		int small = 100000;
		int secsmall = 100000;

		for(int i = 0; i<arr.length; i++){

			if(arr[i] < small){
				secsmall = small;
				small = arr[i];
			}else if (arr[i] < secsmall && arr[i]  != small){
				secsmall = arr[i];
			}
		}
	
		if(secsmall == 100000)
			System.out.println("Smalles = "+small +"Second smallest dosent exists");
		else	
			System.out.println("small = "+small +" Second small = "+secsmall);		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
		Small(arr);	
	}
}
