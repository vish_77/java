/*

   25] Maximum product of two numbers

Given an array Arr of size N with all elements greater than or equal to zero. Return
the maximum product of two numbers possible.
Example 1:
Input:
N = 6
Arr[] = {1, 4, 3, 6, 7, 0}
Output: 42
Example 2:
Input:
N = 5
Arr = {1, 100, 42, 4, 23}
Output: 4200
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
2 ≤ N ≤ 10^7
0 ≤ Arr[i] ≤ 10^4

*/


import java.io.*;
import java.util.Arrays;

class Client{

	static int Prod(int arr[]){
		
		if(arr.length <= 1){
			System.out.println("Array size should be greater than 1");
			return -1;
		}

		Arrays.sort(arr);
		
		return arr[arr.length-1] * arr[arr.length-2];
		
		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
		System.out.println(Prod(arr));	
	}
}
