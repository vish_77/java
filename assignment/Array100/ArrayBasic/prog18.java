/*
18] Find Subarray with given sum | Set 1 (Non-negative
Numbers)
Given an array arr[] of non-negative integers and an integer sum, find a subarray
that adds to a given sum.
Note: There may be more than one subarray with sum as the given sum, print first
such subarray.
Examples:
Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
Output: Sum found between indexes 2 and 4
Explanation: Sum of elements between indices 2 and 4 is 20 + 3 + 10 = 33
Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
Output: Sum found between indexes 1 and 4
Explanation: Sum of elements between indices 1 and 4 is 4 + 0 + 0 + 3 = 7
Input: arr[] = {1, 4}, sum = 0
Output: No subarray found
Explanation: There is no subarray with 0 sum

*/


import java.io.*;

class Client{

	static void SubArray(int arr[],int sum){

		int first = -1;
		int last = -1;

		int subsum = 0;
		int x = 0;

		for(int i = 0; i<arr.length; i++){

			if(subsum > sum){
				subsum = 0;
				x++;
				i=x;
			}

			subsum = subsum + arr[i];
			
			if(subsum == sum){
				System.out.println("Sum found between indexes "+x+" and "+i);
				break;
			}
		
		}	
		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter sum = ");
		int sum = Integer.parseInt(br.readLine());
	
		SubArray(arr,sum);	
	}
}
