/*
2] Find minimum and maximum element in an array

Given an array A of size N of integers. Your task is to find the minimum and
maximum elements in the array.
Example 1:
Input:
N = 6
A[] = {3, 2, 1, 56, 10000, 167}
Output: 1 10000
Explanation: minimum and maximum elements of array are 1 and 10000.

*/


import java.io.*;

class Client{

	static void MinMax(int arr[]){

		if(arr.length == 1){

			System.out.println("Min = "+arr[0] +" Max = "+arr[0]);		
			return;
		}

		int min = arr[0];
		int max = -2147483648;

		for(int i = 0; i<arr.length; i++){

			if(arr[i] > max)
				max = arr[i];
			
			if(arr[i] < min)
				min = arr[i];
		}
			
		System.out.println("Min = "+min +" Max = "+max);		
		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
		MinMax(arr);	
	}
}
