/*
16] Last index of One
Given a string S consisting only '0's and '1's, find the last index of the '1' present in
it.
Example 1:
Input:
S = 00001
Output:
4
Explanation:
Last index of 1 in the given string is 4.

Example 2:
Input:
0
Output:
-1
Explanation:
Since, 1 is not present, so output is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= |S| <= 10^6
S = {0,1}
*/


import java.io.*;

class Client{

	static int LIndex(String str){

		int index = -1;
/*		
		char carr[] = str.toCharArray();


		for(int i = 0; i<carr.length; i++){
			
			if(carr[i] == '1')
				index = i;
	
		}
*/
		for(int i = 0; i<str.length(); i++){

			if(str.charAt(i) == '1')
				index = i;
		}		
		return index;
		
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string = ");
		String str = br.readLine();

		System.out.println("The last index of 1 :"+LIndex(str));	
	}
}
