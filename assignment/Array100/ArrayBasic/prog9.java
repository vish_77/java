/*
9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
Examples 1:
Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
Output: arr[] = { 1, 2, 4, 5 }
Examples 2:
Input: arr[] = { 4, 5, 9, 8, 1 }, index = 3
Output: arr[] = { 4, 5, 9, 1 }
*/


import java.io.*;

class Client{

	static void RemoveEle(int arr[],int index){
		
		if(index >= arr.length)
			System.out.println("Enter proper index");

		else{
			for(int i = index; i<arr.length-1; i++){

				arr[i] = arr[i+1];
				
			}	
		}
	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];	

		System.out.println("Enter array elements = ");
		for(int i = 0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
	
		System.out.println("Enter array index to remove = ");
		int index = Integer.parseInt(br.readLine());

		RemoveEle(arr,index);

		size = size - 1;

		System.out.println("Array:");
		for(int i = 0; i<size; i++){
			System.out.println(arr[i]);
		}
	}
}

