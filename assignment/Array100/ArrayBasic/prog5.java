/*
5] Replace all 0's with 5

You are given an integer N. You need to convert all zeros of N to 5.
Example 1:
Input:
N = 1004
Output: 1554
Explanation: There are two zeroes in 1004
on replacing all zeroes with "5", the new
number will be "1554".
Example 2:
Input:
N = 121
Output: 121
Explanation: Since there are no zeroes in
"121", the number remains as "121".
Expected Time Complexity: O(K) where K is the number of digits in N
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 10000
*/


import java.io.*;

class Client{

	static int Replace(int element){

		int rev = 0;
		while(element != 0){

			int rem = element % 10;

			if(rem == 0)
				rev = rev * 10 + 5;
			else
				rev = rev * 10 + rem;
			
			element /= 10;
		}

		int rev1 = 0;
		while(rev != 0){

			rev1 = rev1 * 10 + (rev%10);
			rev /= 10;
		}
		
		return rev1;

	}


	public static void main(String [] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter an element = ");
		int element = Integer.parseInt(br.readLine());

		element = Replace(element);
		
		System.out.println("Element ="+element);	
	}
}
