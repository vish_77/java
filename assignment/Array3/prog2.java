/*
 
WAP reverse  array element 
IO= 10  244  3  12254
OP = 01 442 3  45221

*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());
	
		int arr[] = new int[size];

		System.out.println("Enter array elements = ");
		for(int i = 0;  i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
  
		for(int i = 0; i<arr.length; i++){
			
		  	int num = arr[i];
			int rev = 0;

			while(num != 0){

				rev = rev * 10 + num%10;
				num /=10;
			}
			System.out.println(rev);
		}


	}
}
		
