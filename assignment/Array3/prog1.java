/*
 
WAP count of digit in array element 
IO= 02  244  3  12254
OP = 2 3 1 5

*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());
	
		int arr[] = new int[size];

		System.out.println("Enter array elements = ");
		for(int i = 0;  i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Digit count = ");

		/*for(int i = 0; i<arr.length; i++){
			
		  	int num = arr[i];
			int count = 0;

			while(num != 0){
				count ++;
				num /= 10;
			}
			System.out.println(count);
		}
		*/

		for(int i =0 ;i<arr.length; i++){

			String str = Integer.toString(arr[i]);

			System.out.println(str.length());
		}
	}
}
		
