/*
 
WAP to find Strong number form array 
print its index

*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());
	
		int arr[] = new int[size];

		System.out.println("Enter array elements = ");
		for(int i = 0;  i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i = 0; i<arr.length; i++){
			
		  	int num1 = arr[i];
			int num2 = num1;
			int count = 0;
			int sum = 0;

			while(num1 != 0){
				count++;
				num1/=10;
			}

			while(num2!=0){
				int mult = 1;
				int rem = num2 % 10;

				for(int j = 1; j<=count; j++){
					mult = mult * rem;
				}
				sum += mult;
				num2/=10;
				
			}

			if(sum == arr[i])
				System.out.println("Armstrong number "+arr[i]+" fount at index : "+i);
			
		}

	}
}
		
