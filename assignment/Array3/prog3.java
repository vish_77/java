/*
 
WAP to find composite number form array 
return ints index

*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());
	
		int arr[] = new int[size];

		System.out.println("Enter array elements = ");
		for(int i = 0;  i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i = 0; i<arr.length; i++){
			
		  	int num = arr[i];
			int count = 0;
			
			for(int j = 1; j <= num; j++){
				if(num % j == 0)
					count++;
			}
			if(count > 2)
				System.out.println("Composite number "+num+"fount at index "+i);
			
		}

	}
}
		
