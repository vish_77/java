/*
 
WAP to find perfect number form array 
print its index

6 is perfect 1+2+3 addition of divisors
*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());
	
		int arr[] = new int[size];

		System.out.println("Enter array elements = ");
		for(int i = 0;  i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i = 0; i<arr.length; i++){
			
		  	int num = arr[i];
			int sum = 0;
			
			for(int j = 1; j < num; j++){
				if(num % j == 0)
					sum +=j;
			}
			if(sum == num)
				System.out.println("Perfect number "+num+" fount at index : "+i);
			
		}

	}
}
		
