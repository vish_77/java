/*
 
WAP to find Strong number form array 
print its index

*/

import java.io.*;
class Demo{
	
	static int smax;
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());
	
		int arr[] = new int[size];

		System.out.println("Enter array elements = ");
		for(int i = 0;  i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
	/*	int max = arr[0];
	//	int smax = 0;
		for(int i = 1; i<arr.length; i++){
			
			if(max < arr[i]){
				smax = max;
				max = arr[i];
			}
		}
		System.out.println("Second maximum element is : "+smax);
*/

		if(size > 2){

			for(int i = 0 ;i<arr.length; i++){

				for(int j = i+1; j<arr.length; j++){
	
					if(arr[i] < arr[j]){
						int tem = arr[i];
						arr[i] = arr[j];
						arr[j] = tem;
					}
				}
			}
			System.out.println("Second minimum element = "+arr[size-2]);
		}else{
			System.out.println("Enter size greater than 2");
		}
	}
}
							
