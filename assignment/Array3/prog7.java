/*
 
WAP to find Strong number form array 
print its index

*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter array size = ");
		int size = Integer.parseInt(br.readLine());
	
		int arr[] = new int[size];

		System.out.println("Enter array elements = ");
		for(int i = 0;  i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i = 0; i<arr.length; i++){
			
		  	int num = arr[i];
			int sum = 0;

			while(num != 0){

				int rem = num % 10;
				int mult = 1;
				for(int j = 1; j<= rem; j++){
				       mult = mult * j;
				}

				sum += mult;
				num /= 10;

			}
			if(sum == arr[i])
				System.out.println("Strong number "+arr[i]+" fount at index : "+i);
		}
		
	}
}
		
