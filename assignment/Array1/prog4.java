/* take character array from user print the vovels */


import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size = ");
		int size = Integer.parseInt(br.readLine());

		char carr[] = new char[size];
		
		System.out.println("Enter array elements");

		for(int i = 0; i<carr.length; i++){

			carr[i] = br.readLine().charAt(0);

		}

		for(int i =0; i<carr.length; i++){

			if(carr[i] == 'a' || carr[i] == 'e' || carr[i] == 'i' || carr[i] == 'o' || carr[i]== 'u')
				System.out.println(carr[i]);
		}
	}
}

