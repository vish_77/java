/* take integer array from user print the product of even numbers */


import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size = ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter array elements");
		int prod = 1;
		for(int i = 0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] % 2 == 0)
				prod = prod * arr[i];
		}
		System.out.println("Product of even elements = "+ prod);
	}
}

