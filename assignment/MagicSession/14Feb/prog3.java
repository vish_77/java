
/*

5 4 3 2 1 
8 7 6 5 
9 8 7 
8 7 
5

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter rows = ");
		int row = Integer.parseInt(br.readLine());
		
		int x = 1;
		int y = row;
		for(int i = 1; i<= row; i++){
			int num = x*y;
			for(int j = row; j>=i; j--){

				System.out.print(num+" ");			
				num--;
			}
			x++;
			y--;
			System.out.println();
		}
	}
}
