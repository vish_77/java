
/*
 
O	
14	13	
L	K	J	
9	8	7	6	
E	D	C	B	A

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter rows = ");
		int rows = Integer.parseInt(br.readLine());
		
		int num = (rows*(rows+1))/2;
		int x = rows;
		for(int i = 1; i<= rows; i++){
			
			for(int j = 1; j<=i; j++){
				
				if(x%2 == 1)
					System.out.print((char)(num+64)+"\t");
				else
					System.out.print(num+"\t");

				num--;				
			}
			x--;		
			System.out.println();
		}
	}
}
