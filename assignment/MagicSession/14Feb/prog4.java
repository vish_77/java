/*
 
 Take start and end from user
 Print the - even number in reverse order
 	   - Odd number in stardard
*/

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter start = ");
		int start = Integer.parseInt(br.readLine());

		System.out.print("Enter start = ");
		int end = Integer.parseInt(br.readLine());

		System.out.print("Even = ");
		for(int i = end; i>= start; i--){
			if(i%2==0)
				System.out.println(i);
		}

		System.out.print("Odd = ");
		for(int i = start; i<= end; i++){
			if(i%2 ==1)
				System.out.println(i);
		}
	}
}

		
