
/*

take two character from user 
1. if characters are same then print a character
2. if characters are different then print the difference between them.

*/

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a char = ");
		char ch1 = br.readLine().charAt(0);

		System.out.println("Enter a char = ");
		char ch2 = br.readLine().charAt(0);

		if(ch1==ch2)
			System.out.println("Both are equal ="+ch1);
		else if(ch1 > ch2)
			System.out.println("Difference = "+(ch1-ch2));
		else
			System.out.println("Difference = "+(ch2-ch1));

	}
}
