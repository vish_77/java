/*

Take number from user and make sum of factorial of digit of number

*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter num = ");
		int num = Integer.parseInt(br.readLine());
		
		int sum = 0;
		while(num != 0){

			int rem = num % 10;
			int mult = 1;
			for(int i = 1; i<=rem; i++){
				mult = mult * i;
			}
			sum = sum + mult;
			num = num / 10;
		}

		System.out.println(sum);
	}
}
