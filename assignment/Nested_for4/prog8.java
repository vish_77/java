
/*
 
10	
I	H	
7	6	5	
D	C	B	A	

*/

class demo{

	public static void main(String[] args){

		int row = 4;
		int num = (row*row+row)/2;

		for(int i = 1; i<=row; i++){
			for(int j=1; j<=i; j++){

				if(i%2==1)
					System.out.print(num+"\t");
				else
					System.out.print((char)(64+num)+"\t");

				num--;

			}
			System.out.println();
		}
	}
}
				
