
/*
 
10	
10	9	
9	8	7	
7	6	5	4

*/

class demo{

	public static void main(String[] args){

		int row = 4;
		int num = (row*row+row)/2;

		for(int i = 1; i<=row; i++){
			for(int j=1; j<=i; j++){

				System.out.print(num--+"\t");
			}
			num++;
			System.out.println();
		}
	}
}
				
