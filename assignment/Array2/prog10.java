/*
Make array from user print the element whose digit sum is even
*/

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array = ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Elements having digit sum even");

		for(int i = 0; i<arr.length; i++){

			int num = arr[i];
			int sum = 0;

			while(num != 0){

				sum = sum + num % 10;
				num = num /10;
			}

			if(sum % 2 == 0)
				System.out.println(arr[i]);
		}

	}
}



