/*
Make array from user print the sum of array element
*/

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array = ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		int sum = 0;
		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){
			arr[i] = sc.nextInt();
			sum += arr[i];
		}

		System.out.println("Sum of array elements = " + sum);
	}
}



