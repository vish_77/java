/*
  Take two array from user print the common elements
*/

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array1 = ");
		int size1 = sc.nextInt();

		int arr1[] = new int[size1];
		
		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr1.length; i++){
			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter the size of array2 = ");
                int size2 = sc.nextInt();

                int arr2[] = new int[size2];

                System.out.println("Enter array elements = ");
                for(int i = 0; i<arr2.length; i++){
                        arr2[i] = sc.nextInt();
                }	
		
		System.out.println("Common array elements = ");
		for(int i = 0; i<size1; i++){

			for(int j = i; j<size2; j++){

				if(arr1[i] != arr2[j])
					System.out.println(arr1[i]);
			}
		}
	}
}
