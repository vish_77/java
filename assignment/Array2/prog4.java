/*
search element in array 
*/

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array = ");
		int size = sc.nextInt();

		int arr[] = new int[size];
	
		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter elements for search = ");
		int ele = sc.nextInt();
		
		int flag = 0;
		int start = 0;
		int end = size - 1;
		while(start < end){
			

			int mid = (start + end)/2;
			System.out.println(mid);
			if(arr[mid] == ele){
				System.out.println("Element found at = "+mid);
				flag = 1;
				break;
			}
			if(ele > arr[mid])
				start = mid+1;

			if(ele < arr[mid])
				end = mid - 1;
		}

		if(flag == 0)
			System.out.println("Element not found");

	}
}



