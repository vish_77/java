/*
Make array from user print the count of odd element and even elements
*/

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array = ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		int evencount = 0;
		int oddcount = 0;
		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){
			arr[i] = sc.nextInt();

			if(arr[i] % 2 == 0)
				evencount++;				
			else
				oddcount++;
		}

		System.out.println("Count of even array elements = " + evencount);
		System.out.println("Count of odd array elements = " + oddcount);
	}
}



