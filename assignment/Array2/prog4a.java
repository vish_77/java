/*
search element in array 
*/

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array = ");
		int size = sc.nextInt();

		int arr[] = new int[size];
	
		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter elements for search = ");
		int ele = sc.nextInt();
		
		int flag = 0;
		for(int i = 0; i<arr.length; i++){
			if(arr[i] == ele){
				System.out.println("Element found at = "+i);
				flag = 1;
			}
		}
		if(flag == 0)
			System.out.println("Element not found");

	}
}



