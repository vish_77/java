/*
  Take two array from user print the common elements
*/

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array1 = ");
		int size1 = sc.nextInt();

		int arr1[] = new int[size1];
		
		System.out.println("Enter array elements = ");
		for(int i = 0; i<arr1.length; i++){
			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter the size of array2 = ");
                int size2 = sc.nextInt();

                int arr2[] = new int[size2];

                System.out.println("Enter array elements = ");
                for(int i = 0; i<arr2.length; i++){
                        arr2[i] = sc.nextInt();
                }	
		
		int size3 = size1 + size2;
		int arr3[] = new int[size3];

		int itr1 = 0;
		while(itr1 < size1){

			arr3[itr1] = arr1[itr1];
			itr1++;
		}

		int itr2 = 0;
		while(itr2 < size2){
			arr3[itr1] = arr2[itr2];
			itr2++;
			itr1++;
		}

		System.out.println("Array 3 elements = ");

                for(int i = 0; i<arr3.length; i++){
			System.out.println(arr3[i]);		
                 }

	}
}
