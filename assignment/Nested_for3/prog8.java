
/*

J	
I	H	
G	F	E	
D	C	B	A	

*/

class demo{

	public static void main(String[] args){

		int row = 4;
		char ch1 = (char)(64+(row*row+row)/2);

		for(int i = 1; i<= row; i++){
			
			for(int j = 1; j<= i; j++){

				System.out.print(ch1--+"\t");
			}
			
			System.out.println();
		}
	}
}
