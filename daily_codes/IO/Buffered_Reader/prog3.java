import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Building name = ");
		String name = br.readLine();

		System.out.println("Enter wing = ");
		char ch = (char)br.read();

		System.out.println(name);
		System.out.println(ch);
		

	}
}
