import java.io.*;

class InputDemo{

	public static void main(String[] args)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter your name = ");
		String name = br.readLine();

		System.out.println("Enter you rollno = ");
		int age = Integer.parseInt(br.readLine());

		System.out.println("Enter your marks = ");
		float marks = Float.parseFloat(br.readLine());

	}
}
