
import java.io.*;
class InputDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string = ");
		String str = br1.readLine();

		br1.close();

		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String = ");
		
		String str3 = br2.readLine();		//Exception in thread "main" java.io.IOException: Stream closed
		
	}
}
