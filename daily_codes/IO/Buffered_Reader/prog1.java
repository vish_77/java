import java.io.*;

class InputDemo{

	public static void main(String[] args){

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter your name = ");

		String name = br.readLine();		//error: unreported exception IOException; must be caught or declared to be thrown
	}
}
