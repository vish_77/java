
/*
	
   Integer as input
   print fizz if divisible by 3
   print buzz if divisible by 5
   print fizz-buzz if divisible by 3 and 5
   print not divisible by both if not 

*/


class Divisible{

        public static void main(String[] args){

		int x = 15;

		if(x % 3 == 0 && x % 5 == 0){

			System.out.println("Fizz-Buzz");

		}else if(x % 3 == 0){

			System.out.println("Fizz");
		}else if(x % 5 == 0){

			System.out.println("Buzz");
		}else{
			System.out.println("Not divisible");
		}
	}
}
