/*
 
   temperature of person
   >98.6   		=>high
   98.0 <= and <=98.6   => Normal
   <98.0		=> low

*/

class Temp{

        public static void main(String[] args){
	
		float temp = 98.6f;

		if(temp <= 98.0 && temp <= 98.6){
			
			System.out.println("Normal");
		
		}else if(temp > 98.6){

			System.out.println("High");

		}else{

			System.out.println("Low");
		}
	}
}
