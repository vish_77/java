// Fibonachi -> 0  1  1  2  3  5  8  13 ...

class Fibo{

	public static void main(String[] args){

		int num1 = 0;
		int num2 = 1;
		int num3 = 1;

		while(num2 <= 10){

			System.out.println(num1);
			num1 = num2;
			num2 = num3;
			num3 = num1 + num2;
		}
	}
}
