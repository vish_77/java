/*

   take an integer N 
  print odd number from 1 to N

*/

class Number{

        public static void main(String[] args){

		int N = 1;

		while(N <= 10){

			if(N % 2 == 1){

				System.out.println(N);

			}
			N++;
		}
	}
}

