//Automorphic logic 3

class Automorphic{

	public static void main(String[] args){

		int num = 5;
		int sqre = num * num;
		int count1 = 0,count2 = 0;

		while(num != 0){

			count1 ++;

			int rem1 = num % 10;
			int rem2 = sqre % 10;

			if(rem1 == rem2)
				count2 ++;

			num /= 10;
			sqre /= 10;
		}

		if(count1 == count2)
			System.out.println("Is Automorphic");
		else
			System.out.println("Not Automorphic");

	}
}
