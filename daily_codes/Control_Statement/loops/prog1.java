//Give an int N print all its digit.

class Digit{

	public static void main(String[] args){

		int num = 18282;

		while(num != 0){

			System.out.println(num%10);
			num = num/10;

		}
	}
}
