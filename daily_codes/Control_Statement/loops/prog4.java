/*

   Give an integer N
   Print perfect squares tll N
   Perfectt square : An integer whose square root is integer

*/

class PerfectSquare{

	public static void main(String[] args){

		int num = 30;
		int i = 1;

		while(i*i <= 30){
			System.out.println(i*i);
			i++;
		}
	}
}
