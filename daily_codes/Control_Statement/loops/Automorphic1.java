/*

   Automorphic = last digit of squre of a number is equal to that number

   5 = 25
   25 = 625

*/

class Automorphic{

	public static void main(String[] args){

		int num = 12;
		int squre = num * num;
		int rev1 = 0;
		int flag = 0;
		
		while(squre != 0){

			rev1 = rev1 * 10 + (squre % 10);
			int temp1 = rev1;	
						
			int rev2 = 0;
			while(temp1 != 0){

				rev2 = rev2 * 10 +(temp1 % 10);
				temp1 /= 10;
			}

			if(rev2 == num){
				System.out.println("Is Automorphic");
				flag = 1;
				break;
			}
			squre /= 10;
		}
		if(flag == 0)
			System.out.println("Not Automorphic");
		
	}
}
				
