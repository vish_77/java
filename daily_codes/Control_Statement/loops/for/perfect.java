/*

   perfect number = It is sum of factors of that number except that number

*/

class Perfect{

	public static void main(String[] args){

		int num =  6;
		int sum = 0;

		for(int i = 1; i<num; i++){

			if(num % i == 0)
				sum += i;

		}

		if(sum == num)

			System.out.println("Perfect number");
		else
			System.out.println("Not perfect number");
	}
}

