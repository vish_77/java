/* 
   Print armstrong in range.

   Armstrong number ->
   if the sum of digit raised to count of digit is equal to that number.

*/

class Armstrong{

	public static void main(String[] args){
		

		
		for(long i = 1; i<999999999999999999l; i++){
		
			long num = i;
                	long sum = 0;
               	 	long temp1=num,temp2 = num;
                	long count = 0;

			while(temp1 != 0){
				count ++;
				temp1 /= 10;
			}

			while(temp2 != 0){

				long mult = 1;
				long rem = temp2 % 10;

				for(int j = 0; j<count; j++){

					mult = mult * rem;
				}
				sum += mult;
				temp2 /= 10;
			}

			if(sum == num)
				System.out.println(num);

		}
	}
}
