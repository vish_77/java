/* 

   Armstrong number ->
   if the sum of digit raised to count of digit is equal to that number.

*/

class Armstrong{

	public static void main(String[] args){

		int num = 1234;
		int sum = 0;
		int temp1=num,temp2 = num;
		int count = 0;

		while(temp1 != 0){
			count ++;
			temp1 /= 10;
		}

		while(temp2 != 0){

			int mult = 0;
			int rem = temp2 % 10;

			for(int i = 0; i<count; i++){

				mult = mult * rem;
			}
			sum += mult;
			temp2 /= 10;
		}

		if(sum == num)
			System.out.println("Armstrong");
		else
			System.out.println("Not armstrong");

	}
}
