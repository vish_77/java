//armstrong number = sum of cube of digit of a number is equal to that number.

class Armstrong{

	public static void main(String[] args){

		int num = 132;
		int sum = 0;
		int temp = num;

		while(num != 0){

			sum = sum + (num % 10);
			num /= 10;

		}

		if(sum == temp)
			System.out.println("Armstrong");
		else
			System.out.println("Not armstrong");
	}
}
