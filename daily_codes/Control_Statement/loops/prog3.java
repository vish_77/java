/*

   Print multiplication of digit of a number

*/

class Mult{

	public static void main(String[] args){

		int num = 48;
		int mult = 1;

		while(num != 0){

			mult = mult *( num%10);

			num = num/10;
		}
		System.out.println(mult);
	}
}
