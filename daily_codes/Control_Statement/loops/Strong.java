//Strong -> Sum of factorial of number equal to that num 


class Strong{

	public static void main(String[] args){

		int num = 145;
		int temp = num;
		int sum = 0;

		while(num != 0){

			int mult = 1;

			for(int i = 1; i<= num % 10; i++){

				mult *= i;
			}
			sum += mult;

			num /= 10;
		}
		if(sum == temp)
			System.out.println("Is strong number");
		else
			System.out.println("Not strong number");
	
	}
}
