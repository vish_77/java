/*

   Given an integer age as input 
   Print Eligible to vote if the person is eligible to vote

*/

class Vote{

	public static void main(String[] args){

		int age = 15;

		if(age >= 18)
			System.out.println("eligible to vote");
		else
			System.out.println("Not eligible to vote");
		
	}
}
