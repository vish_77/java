class MethodsDemo{

	int x = 10;
	static int y = 20;

	static void fun(){

		System.out.println("In fun");
	}

	void gun(){
		System.out.println("In gun");
	}

	public static void main(String[] args){
		
		System.out.println(x);		//error: non-static variable x cannot be referenced from a static context	
		System.out.println(y);	
		fun();
		gun();		//error: non-static method gun() cannot be referenced from a static context

	}
}
