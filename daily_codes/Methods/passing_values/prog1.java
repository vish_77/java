
import java.util.Scanner;
class Demo{


	static void add(int a, int b){

		System.out.println("Add = "+(a+b));
	}
	
	static void sub(int a, int b){

		System.out.println("sub = "+(a-b));
	}
	
	static void mult(int a, int b){

		System.out.println("Mult = "+(a*b));
	}
	
	static void div(int a, int b){

		System.out.println("div = "+(a/b));
	}
	
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter values");

		int a = sc.nextInt();
		int b = sc.nextInt();

		add(a,b);
		sub(a,b);
		mult(a,b);
		div(a,b);
	}
}
