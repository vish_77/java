class Demo{

	void fun(float x){

		System.out.println(x);
	}

	public static void main(String[] args){

		Demo obj = new Demo();

		obj.fun(10);		//10.0
		obj.fun(10.5f);		//10.5
		obj.fun('A');		//65.0
		obj.fun(true);		//error: incompatible types: boolean cannot be converted to float
		obj.fun(14.5);		//error: incompatible types: possible lossy conversion from double to float
		
	}
}

