class MethodsDemo{

	int x = 10;
	static int y = 20;

	static void fun(){

		System.out.println("In fun");
	}

	void gun(){
		System.out.println("In gun");
	}

	public static void main(String[] args){
		
		MethodsDemo obj = new MethodsDemo();		

		System.out.println(obj.x);	//10
		System.out.println(y);		//20

		fun();				//In fun
		obj.gun();			//In gun

	}
}
