class MethodsDemo{

	int x = 10;
	static int y = 20;

	//have to create obj for access non static variable
	static void fun(){
		MethodsDemo obj = new MethodsDemo();
		System.out.println("In fun");
		System.out.println(obj.x);	//10
		System.out.println(y);		//20
	}

	// instance method can access static as well as non static variable directly
	void gun(){		
		System.out.println("In gun");
		System.out.println(x);		//10
		System.out.println(y);		//20
	}

	public static void main(String[] args){
		
		MethodsDemo obj = new MethodsDemo();		
		fun();				
		obj.gun();			
	}
}
