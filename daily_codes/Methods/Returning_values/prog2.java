class Demo{

	void fun(int x){
		return x+10;		//error: incompatible types: unexpected return value
	}

	public static void main(String[] args){

		Demo obj = new Demo();

		System.out.println(obj.fun(10));	//error: 'void' type not allowed here
	}
}
