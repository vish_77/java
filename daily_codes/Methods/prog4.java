class Demo{
	public static void main(String[] args){

		Demo obj = new Demo();
		obj.fun(10);
	}

	void fun(){

		System.out.println("In fun");
	}
}

/*

   OP => required: no arguments
   	 found: int
  	 reason: actual and formal argument lists differ in length

*/


		
