class Core2web{
        
        public static void main(String[] args){
                
		int x = 10;
		{
			int x = 20;
			System.out.println(x);		//Error : variable x is already defined
		}
		{
			int x = 30;
			System.out.println(x);		//Error : variable x is already defined
		}
		System.out.println(x);
	}
}
