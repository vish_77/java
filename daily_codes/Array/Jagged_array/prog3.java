
//Take Jagged array from user and also take its value elments.

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row = ");
		int row = Integer.parseInt(br.readLine());

		int arr[][] = new int[row][];

		for(int i = 0; i<arr.length; i++){

			System.out.println("Enter size of column for row "+i+" :");
			int cols = Integer.parseInt(br.readLine());
			arr[i] = new int[cols];
		}
		
		System.out.println("Enter elements : ");
		for(int i = 0; i<arr.length; i++){

			for(int j = 0; j<arr[i].length; j++){

				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		System.out.println("Array : ");
		for(int x[] : arr){
			for(int y : x){
				System.out.print(y+"\t");
			}
			System.out.println();
		}
	}
}
