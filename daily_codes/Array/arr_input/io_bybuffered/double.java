
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size");
		int size = Integer.parseInt(br.readLine());

		double arr[] = new double[size];

		System.out.println("Enter array elements");
		for(int i = 0; i<arr.length; i++){

			arr[i] = Double.parseDouble(br.readLine());
		

		}

		for(int i = 0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}
}
