class Demo{

	public static void main(String[] args){

		int arr1[] = {10,20,30,40,50};
		char arr2[] = {'A','B','C'};
		float arr3[] = {10.5f,20.5f};
		boolean arr4[] = {true,false,true};
		
		System.out.println("Integer array = ");
		for(int i = 0; i<arr1.length; i++){
			System.out.println(arr1[i]);
		}
		
		System.out.println("Characher array = ");
		for(int i = 0; i<arr2.length; i++){
			System.out.println(arr2[i]);
		}
		
		System.out.println("float array = ");
		for(int i = 0; i<arr3.length; i++){
			System.out.println(arr3[i]);
		}

		System.out.println("boolean array = ");
		for(int i = 0; i<arr4.length; i++){
			System.out.println(arr4[i]);
		}
	}
}
