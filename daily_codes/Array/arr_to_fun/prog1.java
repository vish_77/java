class Demo{

	static void fun(int x, int y){
		System.out.println(x);
		System.out.println(x);

		x = x + 10;
		y = y + 10;

		System.out.println(x);
		System.out.println(y);
	}

	public static void main(String[] args){

		int x = 10;
		int y = 20;

		System.out.println(x);
		System.out.println(y);

		fun(x,y);

		System.out.println(x);
		System.out.println(y);
	}
}

/*
10
20
10
10
20
30
10
20
*/
