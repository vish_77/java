//address of array

class Demo{

	public static void main(String[] args){

		int arr[][] = new int[2][2];	//If we doesnot initialize array bydefault values are 0

		arr[0][0] = 10;
		arr[0][1] = 10;
		arr[1][0] = 10;
		arr[1][1] = 10;

		System.out.println(arr[0][0]);		//10
		System.out.println(arr[0]);		//[I@6504e3b2
		System.out.println(arr[1]);		//[I@515f550a
		System.out.println(arr);		//[[I@626b2d4a
	}
}
