//2D Array decleration

class Demo{
	public static void main(String[] args){
		
		//Type 1
		int arr[][] = new int[][]{{1,2,3},{3,4,5},{6,7,8}};

		//Type2
		int arr2[][] = new int[2][3];
		arr2[0][0] = 10;
		arr2[0][1] = 20;
		arr2[0][2] = 30;
		arr2[1][0] = 40;
		arr2[1][1] = 50;
		arr2[1][2] = 60;

		for(int x[] : arr){
			for(int y:x){
				System.out.print(y+" ");
			}
			System.out.println();
		}

		for(int x[]:arr2){
			for(int y:x){
				System.out.print(y+" ");
			}
			System.out.println();
		}
	}
}



