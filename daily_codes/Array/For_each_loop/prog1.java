class Demo{
	public static void main(String[] args){

		int arr[] = {10,20,30,40,50};
		
		//proper for each loop
		for(int x : arr){
			System.out.println(x);
		}

		//tryal
		for(float y :arr){
			System.out.println(y);
		}
	}
}
