class Demo{

	public static void main(String[] args){

		int a = 10;
		int b = 10;
		int c = 10;
		Integer d = 10;

		Integer x = new Integer(10);

		System.out.println(System.identityHashCode(a));
		System.out.println(System.identityHashCode(b));
		System.out.println(System.identityHashCode(c));
		System.out.println(System.identityHashCode(d));
		System.out.println(System.identityHashCode(x));
	}
}
