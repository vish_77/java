class MyThread extends Thread{

	MyThread(ThreadGroup tg, String str){
		
		super(tg,str);
	}

	public void run(){

		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{

	public static void main(String[] args){

		ThreadGroup pThreadGroup = new ThreadGroup("Core2Web");
		
		MyThread obj1 = new MyThread(pThreadGroup,"C");
		MyThread obj2 = new MyThread(pThreadGroup,"Python");
		MyThread obj3 = new MyThread(pThreadGroup,"Java");
		
		obj1.start();
		obj2.start();
		obj3.start();
	
		ThreadGroup cThreadGroup = new ThreadGroup(pThreadGroup,"Incubator");

		MyThread obj4 = new MyThread(cThreadGroup,"Flutter");
		MyThread obj5 = new MyThread(cThreadGroup,"React");

		obj4.start();
		obj5.start();
	}
}

/*
Thread[C,5,Core2Web]
Thread[Python,5,Core2Web]
Thread[Java,5,Core2Web]
Thread[Flutter,5,Incubator]
Thread[React,5,Incubator]
*/
