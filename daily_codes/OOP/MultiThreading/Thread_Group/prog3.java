class MyThread implements Runnable{
	
	public void run(){

		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{

	public static void main(String[] args){

		ThreadGroup pThreadGP = new ThreadGroup("Alphabet");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pThreadGP,obj1,"Owner");
		Thread t2 = new Thread(pThreadGP,obj2,"Office");

		t1.start();
		t2.start();

		ThreadGroup cThreadGP1 = new ThreadGroup(pThreadGP,"Google");

		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();

		Thread t3 = new Thread(cThreadGP1,obj3,"Browser");
		Thread t4 = new Thread(cThreadGP1,obj4,"Assistant");

		t3.start();
		t4.start();

		ThreadGroup cThreadGP2 = new ThreadGroup(pThreadGP,"Wymo");

		MyThread obj5 = new MyThread();
		MyThread obj6 = new MyThread();

		Thread t5 = new Thread(cThreadGP2,obj5,"cars");
		Thread t6 = new Thread(cThreadGP2,obj6,"office");
	
		t5.start();
		t6.start();

	}
}


