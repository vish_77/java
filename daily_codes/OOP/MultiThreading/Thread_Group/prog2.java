class MyThread extends Thread{

	MyThread(ThreadGroup tg, String str){

		super(tg,str);
	}
	
	public void run(){

		System.out.println(Thread.currentThread());
	
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{

	public static void main(String[] args){

		ThreadGroup pThreadGroup = new ThreadGroup("India");

		MyThread t1 = new MyThread(pThreadGroup,"Maha");
		MyThread t2 = new MyThread(pThreadGroup,"Goa");

		t1.start();
		t2.start();

		ThreadGroup cThreadGroup1 = new ThreadGroup(pThreadGroup,"Pakistan");
		
		MyThread t3 = new MyThread(cThreadGroup1,"Karachi");
		MyThread t4 = new MyThread(cThreadGroup1,"Lahore");

		t3.start();
		t4.start();

		ThreadGroup cThreadGroup2 = new ThreadGroup(pThreadGroup,"Bangaladesh");

		MyThread t5 = new MyThread(cThreadGroup2,"Dakha");
		MyThread t6 = new MyThread(cThreadGroup2,"Mirpur");

		t5.start();
		t6.start();

		cThreadGroup1.interrupt();
		t1.interrupt();

		System.out.println(pThreadGroup.activeCount());
		System.out.println(pThreadGroup.activeGroupCount());
		System.out.println(cThreadGroup1.getParent());
		System.out.println(cThreadGroup1.isDaemon());

		Thread tarr[] = new Thread[]{t1,t2,t3,t4,t5,t6};
//		System.out.println(pThreadGroup.enumerate(tarr));
	}
}
