class MyThread extends Thread{

	public void run(){

		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());

	}
}

class ThreadDemo{

	public static void main(String[] args){

		Thread t = Thread.currentThread();
		
		System.out.println(t);
		System.out.println(t.getPriority());

		MyThread obj = new MyThread();
		obj.start();
		
//		obj.start();			//java.lang.IllegalThreadStateException

		t.setPriority(7);
//		t.setPriority(11);		//java.lang.IllegalArgumentException

		MyThread obj2 = new MyThread();
		obj2.start();
	}
}

/*

Thread[main,5,main]
5
5
7
*/
