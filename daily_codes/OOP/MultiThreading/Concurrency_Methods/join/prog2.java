//This is scenario of deadlock

class MyThread extends Thread{

	static Thread MainThr = null;
	
	public  void run(){

		try{
			MainThr.join();
		}catch(InterruptedException ie){

		}

		for(int i = 0; i<10; i++){
			System.out.println("in Thread0");
		}
	}
}

class ThreadDemo{

	public static void main(String[] args)throws InterruptedException{
	
		MyThread.MainThr = Thread.currentThread();

		MyThread obj = new MyThread();
		obj.start();

		obj.join();
		
		for(int i = 0; i<10; i++){
			System.out.println("in main");
		}
	}
}
