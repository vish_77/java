
class MyThread extends Thread{

	MyThread(String str){

		super(str);
	}

	MyThread(){

	}
	
	public void run(){

		System.out.println(getName());
	}
}

class ThreadDemo{

	public static void main(String[] args){

		MyThread obj = new MyThread("Vishal");		//naming thread while creating
		obj.start();					//Vishal
		
		MyThread obj1 = new MyThread();
		obj1.setName("Vishal");				//naming thread after creation
		obj1.start();					//Vishal
		
		MyThread obj2 = new MyThread();
		obj2.start();					//Thread-1
		
		MyThread obj3 = new MyThread();
		obj3.start();					//Thread-2
	}
}

