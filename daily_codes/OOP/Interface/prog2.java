interface Demo{

        void gun();             //public abstract void gun();
        void fun();             //public abstract void fun();

}
abstract class Child1 implements Demo{

	public void gun(){

		System.out.println("In gun");
	}
}

class Child2 extends Child1{

	public void fun(){

		System.out.println("In fun");
	}
}

class Client{

	public static void main(String[] args){

//		Demo obj = new Demo();		//Demo is abstract; cannot be instantiated

		Demo obj = new Child2();
		obj.fun();			//In fun
		obj.gun();			//In gun
	}
}
