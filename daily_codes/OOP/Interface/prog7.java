interface Demo1{

	default void fun(){
		
		System.out.println("Demo1 - fun");
	}
}

interface Demo2{

	default void fun(){
		
		System.out.println("Demo2 - fun");
	}
}

class DemoChild implements Demo1,Demo2{		//error: types Demo1 and Demo2 are incompatible;
						//class DemoChild inherits unrelated defaults for fun() from types Demo1 and Demo2


}

class Client{

	public static void main(String[] args){

		DemoChild obj1 = new DemoChild();
		obj1.fun();

	}
}
