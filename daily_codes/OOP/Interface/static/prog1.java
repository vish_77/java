interface Demo{

	static void fun(){

		System.out.println("In fun");
	}
}

class DemoChild implements Demo{


}

class Client {

	public static void main(String[] args){

		DemoChild obj = new DemoChild();
//		obj.fun();			//error: cannot find symbol
	
		Demo obj1 = new DemoChild();
//		obj1.fun();			//error: illegal static interface method call
						//the receiver expression should be replaced with the type qualifier 'Demo'
	
		Demo.fun();			//In fun = compulsury called by referance 

	}
}

