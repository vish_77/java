
//can give body in methods of intefrace form java 1.8 version

interface Demo{

	void fun();

	default void gun(){

		System.out.println("In demo gun");

	}

	static void run(){

		System.out.println("In demo run");

	}
}

class Child implements Demo{

	public void fun(){

		System.out.println("In child fun");
	}
}

class Client {

	public static void main(String[] args){

		Demo obj= new Child();
		obj.fun();		//In demo fun
		obj.gun();		//In demo gun
		Demo.run();		//In demo run
	}
}


