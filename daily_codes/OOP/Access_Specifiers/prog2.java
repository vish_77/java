class Employee{

	public int empId = 10;		//scope across the folder
	String str = "kanha";		//scope between the folder
	private int age =  20;		//scope between the class

	void empInfo(){

		System.out.println(empId);
		System.out.println(str);
		System.out.println(age);
	}
}

class MainDemo{

	public static  void main(String[] args){

		Employee emp1 = new Employee();
		emp1.empInfo();
	}
}
