class c2w{

	int numCourses = 4;
	private String fcourse = "Bootcamp";

	void disp(){
		System.out.println(numCourses);
		System.out.println(fcourse);
	}
}
class student{

	public static void main(String[] args){

		c2w obj = new c2w();

		obj.disp();

		System.out.println(obj.numCourses);
		System.out.println(obj.fcourse);	//error: fcourse has private access in c2w
	}
}
