class Demo{

	int x = 10;
	void fun(){
		System.out.println("In fun");
	}
}

class Client{

	public static void main(String[] args){

		Demo obj = new Demo(){

			class abc{
				int a = 20;
				final static int b = 30;

				void gun(){

					System.out.println("IN gun");

					class bca{
						void x (){
							System.out.println("in x");
						}

					}
					bca obj1 = new bca();
					obj1.x();
				}

				
			}
			int x = 10;
			final static int y = 20;

			void fun(){
				System.out.println("in inner fun");
				abc obj = new abc();
				obj.gun();

			}
		};
		obj.fun();
	}
}


/*
in inner fun
IN gun
in x
*/
