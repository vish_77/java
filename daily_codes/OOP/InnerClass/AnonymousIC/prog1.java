class Demo{

	void marry(){

		System.out.println("Kriti Sanon");
	}
}
class Client{

	public static void main(String[] args){

		Demo obj = new Demo(){
			
			void marry(){
				System.out.println("Katrina kaif");
			}
		};
		obj.marry();				//Katrina kaif	
		
		Demo obj1 = new Demo(){

			void marry(){
				System.out.println("Disha Patani");
			}

			Demo fun(){

				System.out.println("in fun");
				return new Demo();
			}

		}.fun();

		obj1.marry();
	}
}

/*
katrina kaif
in fun
kriti sanan

*/
