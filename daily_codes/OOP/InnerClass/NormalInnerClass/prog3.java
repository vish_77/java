class Outer{

	int x = 10;
	static int y = 20;


	class Inner{

		int x = 30;

		void fun2(){

			System.out.println(Outer.this.x);	//10
			System.out.println(this.x);		//30
			
			System.out.println("this$0 = "+Outer.this);		//this$0 = Outer@7cef4e59
			System.out.println("this = "+Inner.this);		//this = Outer$Inner@5e91993f
			System.out.println(this);				//this = Outer$Inner@5e91993f
			
		}
	}

	void fun1(){

			System.out.println("Fun1-Inner");
	}
}

class Client{

	public static void main(String[] args){

		Outer a = new Outer();
		Outer.Inner obj1 = a.new Inner();
		obj1.fun2();
	}
}
