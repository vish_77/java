class Outer{

	class Inner{

		void fun2(){
			System.out.println("In fun2-Inner");
		}
	}
	void fun1(){
		System.out.println("In fun1-Outer");
	}
}

class Client{

	public static void main(String[] args){

		Outer obj = new Outer();			//Outer(obj)
		obj.fun1();

		Outer.Inner obj1 = obj.new Inner();		//Outer$Inner(obj1,obj)
		obj1.fun2();

		Outer.Inner obj2 = obj.new Inner();		//Outer$Inner(obj2,obj)
		obj2.fun2();

		Outer.Inner obj3 = new Outer().new Inner();	//Outer$Inner(obj3,new Outer())
		obj3.fun2();

	}
}

/*
In fun1-Outer
In fun2-Inner
In fun2-Inner
In fun2-Inner
*/
