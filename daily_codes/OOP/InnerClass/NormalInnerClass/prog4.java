class Outer{

	int x = 10;
	static int y = 20;

	class Inner{

		int a = 30;
//		static int b = 30;	//error: Illegal static declaration in inner class Outer.Inner
		
		final static int b = 30;
	}

}

class Client{

	public static void main(String[] args){

		System.out.println(Outer.y);			//20

		Outer obj = new Outer();
		System.out.println(obj.x);			//10

		System.out.println(obj.new Inner().b);		//30
	}
}



