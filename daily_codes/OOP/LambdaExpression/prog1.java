
interface Core2Web{

	void lang();
}

class Year2022{

	public static void main(String[] args){

		Core2Web c2w = () -> {

			System.out.println("Bootcamp/Java/Python/OS");
		};

		Core2Web xyz = () -> System.out.println("Vishal");

		c2w.lang();
		xyz.lang();
	}
}
