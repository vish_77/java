
interface House{

	String fun(int num);
}

class Demo{

	public static void main(String[] args){

		House h1 = (num) -> "Bootcamp/Java/Py"+":"+num;
		House h2 = (int num) -> "Bootcamp/Java/Py"+":"+num;
		House h3 = num -> "Bootcamp/Java/Py"+":"+num;

		System.out.println(h1.fun(20));
		System.out.println(h2.fun(30));
		System.out.println(h1.fun(40));

	}
}

/*
Bootcamp/Java/Py:20
Bootcamp/Java/Py:30
Bootcamp/Java/Py:40
*/
