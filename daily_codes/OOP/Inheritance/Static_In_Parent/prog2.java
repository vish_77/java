class Parent{
	
	static int x = 10;

	static {
		System.out.println("In parent static Block");
	}

	static void access(){

		System.out.println(x);
	}

}

class Child extends Parent{

	static {

		System.out.println("In child static Block");
		System.out.println(x);
		access();
	}
}
class Client{

	public static void main(String[] args){
		
		System.out.println("In main");
		Child obj = new Child();

	}
}

/*
In main
In parent static Block
In child static Block
10
10
*/
