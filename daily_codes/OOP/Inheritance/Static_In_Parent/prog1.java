class Parent{

	static {
		System.out.println("In parent static Block");
	}

}

class Child extends Parent{

	static {

		System.out.println("In child static Block");
	}
}
class Client{

	public static void main(String[] args){

		Child obj = new Child();

	}
}

/*
In parent static Block
In child static Block
*/
