class Parent{

	int x = 10;
	static int y = 20;

	Parent(){

		System.out.println("Parent constructor");

	}

	void fun(){

		System.out.println("parent fun");
	}
}

class Child extends Parent{

	int x = 100;
	static int y = 200;

	Child(){

		System.out.println("Child constructor");
	}

	void access(){
		
//		super();		//error: call to super must be first statement in constructor

		System.out.println(super.x);		//10
		System.out.println(super.y);		//20
		System.out.println(this.x);		//100
		System.out.println(this.y);		//200
		System.out.println(x);			//100
		System.out.println(y);			//200
	}

	void fun(){
		
		super.fun();
		System.out.println("Child fun");
		
	}
}

class Client{

	public static void main(String[] args){

		Child obj = new Child();
		obj.access();
		obj.fun();
	}
}

/*
Parent constructor
Child constructor
10
20
100
200
100
200
parent fun
Child fun
*/
