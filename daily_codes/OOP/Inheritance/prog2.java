class Parent{

	Parent(){

		System.out.println("In Parent constructor");
	}

	void ParentProperty(){

		System.out.println("farm, gold");
	}
}

class Child extends Parent{

	Child(){
		System.out.println("In child constructor");
	}
}

class Client{

	public static void main(String[] args){

		Child obj = new Child();
		obj.ParentProperty();
	}
}

/*
In Parent constructor
In child constructor
farm, gold
*/
