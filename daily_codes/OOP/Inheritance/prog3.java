class Parent{

	int x = 10;
	Parent(){

		System.out.println("In Parent Constructor");
	}

	void access(){

		System.out.println("Parent Instance");
	}
}

class Child extends Parent{

	int y = 20;
	Child(){

		System.out.println("In child Construcotr");
		System.out.println(x);
		System.out.println(y);
	}
}

class Client{

	public static void main(String[] args){

		Child obj = new Child();
		obj.access();
	}
}

/*
In Parent Constructor
In child Construcotr
10
20
Parent Instance
*/
