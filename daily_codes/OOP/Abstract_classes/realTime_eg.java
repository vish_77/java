abstract class ReserveBank{

	void Rules(){

		System.out.println("Rules for bank");
	}
	
	abstract void InterastRate();
}

class HDFC extends ReserveBank{

	void InterastRate(){

		System.out.println("HDFC interast rate");
	}
}

class Client{

	public static void main(String[] args){

		HDFC obj = new HDFC();

		obj.Rules();		//Rules for bank
		obj.InterastRate();	//HDFC interast rate
	}
}


