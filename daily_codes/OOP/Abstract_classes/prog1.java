abstract class Parent{

	void career(){

		System.out.println("Doctor");
	}

	abstract void marry();
}

abstract class Child1 extends Parent{
	
						//if we are not giving body to the abstract methods then
						//we should declare the class abstract
}

class Child2 extends Child1{

	void marry(){

		System.out.println("Kiyara");
	}
}

class Client{

	public static void main(String[] args){

//		Parent obj = new Parent();	//error: Parent is abstract; cannot be instantiated
		Child obj1 = new Child();

		Parent obj2 = new Child();
		obj2.career();			//Doctor
		obj2.marry();			//Kiyara

	}
}
