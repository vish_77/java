
//we can write extra unchecked(runtime) exceptions

import java.io.*;
class Demo{

	public static void main(String[] args){

		try{
			int x = 10/0;

		}catch(ArithmeticException obj){
			
			System.out.println("can't divide by zero");
		
		}catch(NullPointerException obj2){

		}catch(NumberFormatException obj3){

		}
	}
}

