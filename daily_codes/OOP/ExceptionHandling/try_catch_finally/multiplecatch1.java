
//in multiple catch child exception followed by parent not reverse or random

import java.util.*;

class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int x = 0;
		try{
			x = sc.nextInt();
	
		}catch(IllegalArgumentException obj){

			System.out.println("Not entered integer");

		}catch(NumberFormatException obj1){
			
			System.out.println("Not entered integer");
		}
	
		System.out.println(x);

	}
}

//error: exception NumberFormatException has already been caught
