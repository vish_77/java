
import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException,InterruptedException,ArithmeticException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();

		for(int i = 0; i<10; i++){

			System.out.println(i);
			Thread.sleep(500);
		}

		int x = 10/0;

		System.out.println("End Main");

	}
}

/* OP
 
vishal
0
1
2
3
4
5
6
7
8
9
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at Demo.main(prog5.java:17)
*/
