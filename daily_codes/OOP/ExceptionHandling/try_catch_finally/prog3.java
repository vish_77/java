
import java.io.*;
class Demo{

	public static void main(String[] args){

		try{
			int x = 10/0;

		}catch(IOException obj){
			
			System.out.println("can't divide by zero");
		}
	}
}

//error: exception IOException is never thrown in body of corresponding try statement
//we cannot write extra checked exception. hava to write where it actually required
