
import java.util.Scanner;

class InsufficientBankBalanceException extends RuntimeException{

	InsufficientBankBalanceException(String msg){

		super(msg);
	}
}

class Payment{

	public static void main(String[] args){

		int bankbalance = 98;

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Your bankbalance is = "+bankbalance);
		System.out.println("Enter amount to pay");

		int amount = sc.nextInt();

		if(amount > bankbalance){

			throw new InsufficientBankBalanceException("You have insufficient bank balance");
		}

		System.out.println("Payment done");
		
		bankbalance = bankbalance - amount;
		System.out.println("Your bankbalance is = "+bankbalance);
	}
}

	
