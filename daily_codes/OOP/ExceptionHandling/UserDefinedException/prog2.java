
import java.util.Scanner;


class DataOverFlowException extends RuntimeException{

	DataOverFlowException(String msg){

		super(msg);
	}
}

class DataUnderFlowException extends RuntimeException{

	DataUnderFlowException(String msg){

		super(msg);
	}
}

class ArrayDemo{

	public static void main(String[] args){

		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter integer value");
		System.out.println("Note : 0 < element < 100");
		System.out.println("Enter array elemtns");

		for(int i = 0; i<arr.length; i++){

			int data = sc.nextInt();
		
			try{

				if(data < 0){

					throw new DataUnderFlowException("Mitra Data zero peksha lahan ahe");
			//		int x = 20;		//error=unreachable statement
				}
			
			}catch(DataUnderFlowException obj){
			
				System.out.println("Renter data");
				data = sc.nextInt();
			}

			try{

				if(data > 100){
	
					throw new DataOverFlowException("Mitra Data 100 peksha jasta ahe");
				}
			}catch(DataOverFlowException obj){

				System.out.println(obj.getMessage());
				System.out.println("Reenter Data");
				
				data = sc.nextInt();
			}


			arr[i] = data;
		}

		for(int i = 0; i<arr.length; i++){
	
			System.out.print(arr[i] + "  ");
		}
	}
}



