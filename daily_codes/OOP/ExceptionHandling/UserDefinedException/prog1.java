

// Elements in array should be greater than 0 and smaller than 100


import java.util.Scanner;


class DataOverFlowException extends RuntimeException{

	DataOverFlowException(String msg){

		super(msg);
	}
}

class DataUnderFlowException extends RuntimeException{

	DataUnderFlowException(String msg){

		super(msg);
	}
}

class ArrayDemo{

	public static void main(String[] args){

		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter integer value");
		System.out.println("Note : 0 < element < 100");
		System.out.println("Enter array elements");

		for(int i = 0; i<arr.length; i++){

			int data = sc.nextInt();
			
			if(data < 0){

				throw new DataUnderFlowException("Mitra Data zero peksha lahan ahe");
			}

			if(data > 100){

				throw new DataOverFlowException("Mitra Data 100 peksha jasta ahe");
			}

			arr[i] = data;
		}

		for(int i = 0; i<arr.length; i++){
	
			System.out.print(arr[i] + "  ");
		}
	}
}



