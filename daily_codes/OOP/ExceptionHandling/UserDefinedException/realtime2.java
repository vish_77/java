
import java.util.Scanner;

class RainException extends RuntimeException{

	RainException(String msg){

		super(msg);
	}
}

class Client{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Today is class");
		System.out.println("Enter it is raining or not Y or N = ");
		char str = sc.next().charAt(0);

		if(str == 'Y' || str == 'y'){

			throw new RainException("can't go to the class");
		}

		System.out.println("Can go to the class");
	}
}

