
import java.io.*;

class Demo{

	void fun()throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int data = Integer.parseInt(br.readLine());

	}

	public static void main(String[] args)throws IOException{

		Demo obj = new Demo();
		obj.fun();
	}
}

/*
vishal
Exception in thread "main" java.lang.NumberFormatException: For input string: "vishal"
	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.base/java.lang.Integer.parseInt(Integer.java:652)
	at java.base/java.lang.Integer.parseInt(Integer.java:770)
	at Demo.fun(NumberFormat.java:10)
	at Demo.main(NumberFormat.java:17)
*/
