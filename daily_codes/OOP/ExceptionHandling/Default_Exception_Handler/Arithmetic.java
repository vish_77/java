class Demo{


	void m3(){

		System.out.println("Start m3");
		System.out.println(10/0);
		System.out.println("End m3");
	}
	void m2(){

		System.out.println("Start m2");
		m3();
		System.out.println("End m2");

	}

	void m1(){

		System.out.println("Start m1");
		m2();
		System.out.println("End m1");
	}

	public static void main(String[] args){

		System.out.println("Start main");
		
		Demo obj = new Demo();
		obj.m1();

		System.out.println("End main");
	}
}

/*

Start main
Start m1
Start m2
Start m3
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at Demo.m3(prog1.java:7)
	at Demo.m2(prog1.java:13)
	at Demo.m1(prog1.java:21)
	at Demo.main(prog1.java:30)

*/

