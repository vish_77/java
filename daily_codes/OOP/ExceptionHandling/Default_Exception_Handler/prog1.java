
import java.io.*;

class Demo{

	void fun()throws IOException{


		System.out.println("Start fun");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int data = 0;
		System.out.println("Enter integer =");

		try{
			data = Integer.parseInt(br.readLine());

		}catch(NumberFormatException obj){
			
			System.out.println("enter proper input");
			System.out.println(obj.toString());
			System.out.println(obj.getMessage());
			obj.printStackTrace();
		}

		System.out.println("End fun");
		
	}

	public static void main(String[] args)throws IOException{
	
		System.out.println("Start main");
		
		Demo obj = new Demo();	
		obj.fun();

		System.out.println("End main");
	}
}
