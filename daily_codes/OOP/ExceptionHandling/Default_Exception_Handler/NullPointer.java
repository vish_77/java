class Demo{

	void m1(){
		System.out.println("In m1");
	}

	void m2(){
		System.out.println("In m1");
	}

	public static void main(String[] args){
	
		System.out.println("Start Main");

		Demo obj = new Demo();
		obj.m1();

		obj = null;
		obj.m2();

		System.out.println("End Main");
	}
}

/*

Start Main
In m1
Exception in thread "main" java.lang.NullPointerException
	at Demo.main(NullPointer.java:19)

*/
