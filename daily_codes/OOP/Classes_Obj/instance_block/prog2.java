class Demo{

	int x = 10;

	static int y = 20;

	Demo(){
		System.out.println("In constructor");
	}

	static{
		System.out.println("In static block 1");
	}

	{
		System.out.println("In instance block 1");
	}

	public static void main(String[] args){

		Demo obj = new Demo();
		System.out.println("In Main");
	}

	static {
		System.out.println("In static block 2");
	}

	{
		System.out.println("In instance block 2");
	}
}

/*
In static block 1
In static block 2
In instance block 1
In instance block 2
In constructor
In Main
*/
