class Demo{

	int x = 10;

	Demo(){

		System.out.println("Constructor");
	}

	//instance block
	{
		System.out.println("Instance block 1");
	}

	public static void main(String[] args){

		Demo obj = new Demo();
		System.out.println("Main");
	}
	
	//instance block
	{
		System.out.println("Instance block 2");

	}
}

/*
Instance block 1
Instance block 2
Constructor
Main
*/
