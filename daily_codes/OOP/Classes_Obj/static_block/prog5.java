class Demo{

	static int x = 10;

	static{
		int y = 20;	//local variable
		
		static int x = 30;	//error: illegal start of expression
	}

	void fun(){

		static int x = 30;	//error: illegal start of expression

	}

	static void gun(){

		static int x = 30;	//error: illegal start of expression
	}

	public static  void main(String[] args){

		static int x = 30;	//error: illegal start of expression
	}
}
