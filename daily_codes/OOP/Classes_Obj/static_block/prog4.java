class Demo{

	static {
		System.out.println("Static Block1");
	}

	public static void main(String[] args){

		System.out.println("In Demo Main");
	}
}

class Client{

	static{
		System.out.println("Static Block2");
	}
	
	public static void main(String[] args){

		System.out.println("In client Main");
	}
}

/*

  run with -> java Demo op = 	Static Block1
				In Demo Main
	
	    java Client op = 	Static Block2
				In client Main

*/


