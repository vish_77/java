
class Demo{

	int x = 10;
	Demo(){
		this(20);

		System.out.println("In No-args");
	}

	Demo(int x){

		this(20,30);
		System.out.println("In para");
	}

	Demo(int x, int y){

		System.out.println("in 2 para");
	}

	public static  void main(String[] args){

		Demo obj = new Demo();
	}
}

/*
in 2 para
In para
In No-args

*/
