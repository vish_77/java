
class Demo{

	int x = 10;
	Demo(){
		this(20);

		System.out.println("In No-args");
	}

	Demo(int x){

		this();
		System.out.println("In para");
	}

	public static  void main(String[] args){

		Demo obj = new Demo();
	}
}

//error: recursive constructor invocation
