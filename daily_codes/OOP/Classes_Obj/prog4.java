class Employee{

	int empId = 10;
	String name = "Vishal";
	static int y = 50;

	void empInfo(){
		System.out.println(empId);
		System.out.println(name);
		System.out.println(y);
	}
}
class MainDemo{

	public static void main(String[] args){

		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.empInfo();
		emp2.empInfo();

		emp1.empId = 20;
		emp1.name = "Pankaj";
		emp1.y = 3000;
	
		emp1.empInfo();
		emp2.empInfo();
	}
}
/*

10
Vishal
50
10
Vishal
50
20
Pankaj
3000
10
Vishal
3000

*/
