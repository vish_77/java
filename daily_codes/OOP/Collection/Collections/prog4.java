
import java.util.*;

class Cafe{

	String name = null;
	float rating = 0.0f;
	int emp = 0;
	double revenue = 0.0;

	Cafe(String name, float rating, int emp, double revenue){

		this.name = name;
		this.rating = rating;
		this.emp = emp;
		this.revenue = revenue;

	}

	public String toString(){

		return "{ "+name+", "+rating+", "+emp+", "+revenue+" }";
	}
}

class SortByName implements Comparator<Cafe>{

	public int compare(Cafe obj1, Cafe obj2){

		return (obj1.name).compareTo((obj2.name));
	}
}

class SortByRating implements Comparator<Cafe>{

	public int compare(Cafe obj1, Cafe obj2){

		return (int)(obj1.rating - obj2.rating);
	}
}

class SortByEmp implements Comparator<Cafe>{

	public int compare(Cafe obj1, Cafe obj2){

		return obj1.emp - obj2.emp;
	}
}

class SortByRevenue implements Comparator<Cafe>{

	public int compare(Cafe obj1, Cafe obj2){

		return (int)(obj1.revenue - obj2.revenue);
	}
}

class LinkedListDemo{

	public static void main(String[] args){

		LinkedList<Cafe> ll = new LinkedList<Cafe>();

		ll.add(new Cafe("#cafe",4.5f,5,10.65));
		ll.add(new Cafe("Cric",3.5f,15,25.95));
		ll.add(new Cafe("Backlog",2.5f,2,5.55));
		ll.add(new Cafe("Sai",5.0f,3,6.5));

		System.out.println(ll);

		Collections.sort(ll,new SortByName());
		System.out.println(ll);

		Collections.sort(ll,new SortByRating());
		System.out.println(ll);

		Collections.sort(ll,new SortByEmp());
		System.out.println(ll);

		Collections.sort(ll,new SortByRevenue());
		System.out.println(ll);

	}
}
