
import java.util.*;

class SortDemo{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add("Vishal");
		al.add("Pankaj");
		al.add("Shubham");
		al.add("Saurabh");
		al.add("Raj");
		al.add("Vishal");

		System.out.println(al);

		Collections.sort(al);
		
		System.out.println(al);
	}
}

/*

[Vishal, Pankaj, Shubham, Saurabh, Raj, Vishal]
[Pankaj, Raj, Saurabh, Shubham, Vishal, Vishal]

*/
