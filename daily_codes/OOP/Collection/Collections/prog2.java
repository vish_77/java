import java.util.*;

class Employee{

	String name = null;
	float sal = 0.0f;

	Employee(String name, float sal){

		this.name = name;
		this.sal = sal;

	}

	public String toString(){

		return "{"+name+" : "+sal+"}";

	}

}

class SortByName implements Comparator<Employee>{
	
	public int compare(Employee obj1, Employee obj2){

		return obj1.name.compareTo(obj2.name);
	}

}

class SortBySal implements Comparator<Employee>{

	public int compare(Employee obj1, Employee obj2){

		return (int)(obj1.sal - obj2.sal);
	}
}

class ListSortDemo{

	public static void main(String[] args){

		ArrayList<Employee> al = new ArrayList<Employee>();

		al.add(new Employee("Vishal",200000.00f));
		al.add(new Employee("Shubham",150000.00f));
		al.add(new Employee("Pnakaj",120000.00f));

		System.out.println(al);

		Collections.sort(al,new SortByName());
		
		System.out.println(al);

		Collections.sort(al,new SortBySal());

		System.out.println(al);

	}
}

/*

[{Vishal : 200000.0}, {Shubham : 150000.0}, {Pnakaj : 120000.0}]
[{Pnakaj : 120000.0}, {Shubham : 150000.0}, {Vishal : 200000.0}]
[{Pnakaj : 120000.0}, {Shubham : 150000.0}, {Vishal : 200000.0}]

*/
