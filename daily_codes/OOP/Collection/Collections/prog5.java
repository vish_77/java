
import java.util.*;

class Cafe{

	String name = null;
	float rating = 0.0f;
	int emp = 0;
	double revenue = 0.0;

	Cafe(String name, float rating, int emp, double revenue){

		this.name = name;
		this.rating = rating;
		this.emp = emp;
		this.revenue = revenue;

	}

	public String toString(){

		return "{ "+name+", "+rating+", "+emp+", "+revenue+" }";
	}
}

class SortByName implements Comparator{

	 public int compare(Object obj1, Object obj2){

                return (((Cafe)obj1).name).compareTo(((Cafe)obj2).name);
        }
}

class SortByRating implements Comparator{

	public int compare(Object obj1, Object obj2){

		return (int) (((Cafe)obj1).rating - ((Cafe)obj2).rating);
	}
}

class SortByEmp implements Comparator{

	public int compare(Object obj1, Object obj2){

		return ((Cafe)obj1).emp - ((Cafe)obj2).emp;
	}
}

class SortByRevenue implements Comparator{

	public int compare(Object obj1, Object obj2){

		return (int)(((Cafe)obj1).revenue - ((Cafe)obj2).revenue);
	}
}

class LinkedListDemo{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.add(new Cafe("#cafe",4.5f,5,10.65));
		ll.add(new Cafe("Cric",3.5f,15,25.95));
		ll.add(new Cafe("Backlog",2.5f,2,5.55));
		ll.add(new Cafe("Sai",5.0f,3,6.5));

		System.out.println(ll);

		Collections.sort(ll,new SortByName());
		System.out.println(ll);

		Collections.sort(ll,new SortByRating());
		System.out.println(ll);

		Collections.sort(ll,new SortByEmp());
		System.out.println(ll);

		Collections.sort(ll,new SortByRevenue());
		System.out.println(ll);

	}
}
