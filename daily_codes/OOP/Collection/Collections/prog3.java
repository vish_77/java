
import java.util.*;

class Movies{

	String name = null;
	double BoxOff = 0.0;
	float imdb = 0.0f;

	Movies(String name, double BoxOff, float imdb){

		this.name = name;
		this.BoxOff = BoxOff;
		this.imdb = imdb;
	}

	public String toString(){

		return "{ "+name+" : "+BoxOff+" : "+imdb+" }";
	}
}

class SortByName implements Comparator{

	public int compare(Object obj1, Object obj2){

		return (((Movies)obj1).name).compareTo(((Movies)obj2).name);
	}
}

class SortByBoxOff implements Comparator{

	public int compare(Object obj1, Object obj2){

		return (int)(((Movies)obj1).BoxOff - ((Movies)obj2).BoxOff);
	}
}

class SortByimdb implements Comparator{

	public int compare(Object obj1, Object obj2){

		return (int)(((Movies)obj1).imdb - ((Movies)obj2).imdb);
	}
}


class ArrayListDemo{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(new Movies("OMG2",500.0,7.6f));
		al.add(new Movies("Jailer",400.2,6.8f));
		al.add(new Movies("Gadar2",300.4,9.0f));
		al.add(new Movies("Pathan",100.3,5.3f));

		System.out.println(al);				//[{ OMG2 : 500.0 : 7.6 }, { Jailer : 400.2 : 6.8 }, { Gadar2 : 300.4 : 9.0 }, { Pathan : 100.3 : 5.3 }]

		Collections.sort(al,new SortByName());		//[{ Gadar2 : 300.4 : 9.0 }, { Jailer : 400.2 : 6.8 }, { OMG2 : 500.0 : 7.6 }, { Pathan : 100.3 : 5.3 }]
		System.out.println(al);
		
		Collections.sort(al,new SortByBoxOff());	//[{ Pathan : 100.3 : 5.3 }, { Gadar2 : 300.4 : 9.0 }, { Jailer : 400.2 : 6.8 }, { OMG2 : 500.0 : 7.6 }]
		System.out.println(al);

		Collections.sort(al,new SortByimdb());
		System.out.println(al);				//[{ Pathan : 100.3 : 5.3 }, { Jailer : 400.2 : 6.8 }, { OMG2 : 500.0 : 7.6 }, { Gadar2 : 300.4 : 9.0 }]
	}
}
