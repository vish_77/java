
/*
 * ArrayList methods
 * 1. add(E);				=> E - Element
 * 2. int size();
 * 3. boolean contains(object);		=> check the presence of object
 * 4. int indexOf(object);
 * 5. int lastIndexOf(objcet);
 * 6. E get(int);
 * 7. E set(int, E);
 * 8. void add(int,E);
 * 9. E remove(int);
 * 10. boolean remove(Object);
 * 11. void clear();
 * 12. boolean addAll(Collection);
 * 13. boolean addAll(int,Collection);
 * 14. protected void removeRange(int, int);
 * 15. Objcet[] toArray();
 *
 */


import java.util.*;

class ArrayListDemo extends ArrayList{

	public static void main(String[] args){

//		ArrayList al = new ArrayList();

		ArrayListDemo al = new ArrayListDemo();
		
		al.add(10);
		al.add(20.5f);
		al.add("Vishal");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);				//[10, 20.5, Vishal, 10, 20.5]
		
		System.out.println(al.size());			//5

		System.out.println(al.contains("Vishal"));	//ture
		System.out.println(al.contains(50));		//false

		System.out.println(al.indexOf(20.5f));		//1
		System.out.println(al.indexOf(50));		//-1
		System.out.println(al.lastIndexOf(20.5f));	//5

		System.out.println(al.get(2));			//Vishal
		System.out.println(al.set(3,"More"));		//10

		al.add(3,"Core2Web");

		ArrayList al2 = new ArrayList();
		al2.add("Salman");
		al2.add("Sharuk");
		al2.add("Amir");

		al.addAll(al2);
		al.addAll(3,al2);

		Object arr[] = al.toArray();
		for(Object data : arr){
			System.out.println(data);
		}

		System.out.println(al.remove(3));

		al.removeRange(3,5);

		System.out.println(al.remove(20.5f));

		al.clear();
	
		System.out.println(al);	
	}
}

		
		
