/*
 *in java linked list supports index because of List interface is parent
 *
 * Linked List methods
 * 1. E getFirst();
 * 2. E getLast();
 * 3. E removeFrist();
 * 4. E removeLast();
 * 5. void addFirst(E);
 * 6. void addLast(E);
 * 7. java.util.LinkedList$Node node(int);
 */

import java.util.*;
class LinkedListDemo extends LinkedList{

	public static void main(String[] args){

		LinkedListDemo ll = new LinkedListDemo();
		
		ll.add(20);
		ll.addFirst(10);
		ll.addLast(30);

		System.out.println(ll);

		LinkedList.Node obj = ll.new Node();
	}
}
