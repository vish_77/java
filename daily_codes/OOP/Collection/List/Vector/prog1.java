import java.util.*;

class VectorDemo{

	public static  void main(String[] args){

		Vector v = new Vector();
	
		System.out.println(v.capacity());	//10
		
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		v.addElement(10);
		
		System.out.println(v);
		
		System.out.println(v.capacity());	//20
	
		Vector v2 = new Vector(5);
		System.out.println(v2.capacity());

		Vector v3 = new Vector(5,100);
		System.out.println(v2.capacity());
		System.out.println(v3);
		
	}
}
