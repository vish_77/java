import java.util.*;

class SortedMapDemo{

	public static void main(String[] args){

		SortedMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Sl","Srilanka");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");
		
		Set<Map.Entry>data = tm.entrySet();

		Iterator<Map.Entry>itr = data.iterator();

		while(itr.hasNext()){

			//System.out.println(itr.next());

			Map.Entry abc = itr.next();
			System.out.println(abc.getKey()+" : "+abc.getValue());
		}
	}
}
