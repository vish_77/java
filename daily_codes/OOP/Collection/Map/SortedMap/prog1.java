

/*

  public abstract java.util.SortedMap<K, V> subMap(K, K);
  public abstract java.util.SortedMap<K, V> headMap(K);
  public abstract java.util.SortedMap<K, V> tailMap(K);
  public abstract K firstKey();
  public abstract K lastKey();
  public abstract java.util.Set<K> keySet();
  public abstract java.util.Collection<V> values();
  public abstract java.util.Set<java.util.Map$Entry<K, V>> entrySet();

*/

import java.util.*;

class SortedMapDemo{

	public static void main(String[] args){

		SortedMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Sl","Srilanka");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");
	
		System.out.println(tm);	
		//subMap
		System.out.println(tm.subMap("Australia","Pakistan"));

		//headMap
		System.out.println(tm.headMap("Sl"));
		
		//tailMmap
		System.out.println(tm.tailMap("Sl"));

		//firstKey
		System.out.println(tm.firstKey());

		//lastKey
		System.out.println(tm.lastKey());

		//keySet
		System.out.println(tm.keySet());

		//values
		System.out.println(tm.values());

		//entrySet
		System.out.println(tm.entrySet());

	}		
}

