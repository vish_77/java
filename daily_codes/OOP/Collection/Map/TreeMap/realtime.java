
import java.util.*;

class platform implements Comparable{

	String pltName = null;
	int foundYear = 0;

	platform(String pltName, int foundYear){
		
		this.pltName = pltName;
		this.foundYear = foundYear;
	}

	public String toString(){

		return "{"+pltName+":"+foundYear+"}";

	}

	public int compareTo(Object obj){

		return foundYear - ((platform)obj).foundYear;
	}
}

class TreeMapDemo{

	public static void main(String[] args){

		TreeMap tm = new TreeMap();

		tm.put(new platform("Youtube",2005),"Google");
		tm.put(new platform("Insta",2010),"Meta");
		tm.put(new platform("facebook",2004),"Meta");
		tm.put(new platform("ChatGPT",2022),"OpenAI");

		System.out.println(tm);

	}
}
