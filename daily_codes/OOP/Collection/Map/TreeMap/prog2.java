import java.util.*;

class platform{

	String pltName = null;
	int foundYear = 0;

	platform(String pltName, int foundYear){
		
		this.pltName = pltName;
		this.foundYear = foundYear;
	}

	public String toString(){

		return "{"+pltName+":"+foundYear+"}";

	}
}

class SortBypltName implements Comparator{

	public int compare(Object obj1, Object obj2){

		return ((platform)obj1).pltName.compareTo(((platform)obj2).pltName);
	}
}

class SortByfoundYear implements Comparator{

	public int compare(Object obj1, Object obj2){

		return ((platform)obj1).foundYear - ((platform)obj2).foundYear;
	}
}

class TreeMapDemo{

	public static void main(String[] args){

		TreeMap tm = new TreeMap(new SortByfoundYear());

		tm.put(new platform("Youtube",2005),"Google");
		tm.put(new platform("Insta",2010),"Meta");
		tm.put(new platform("Facebook",2004),"Meta");
		tm.put(new platform("ChatGPT",2022),"OpenAI");

		System.out.println(tm);

	 	TreeMap tm1 = new TreeMap(new SortBypltName());

                tm1.put(new platform("Youtube",2005),"Google");
                tm1.put(new platform("Insta",2010),"Meta");
                tm1.put(new platform("Facebook",2004),"Meta");
                tm1.put(new platform("ChatGPT",2022),"OpenAI");

                System.out.println(tm1);

	}
}	
