import java.util.*;

class IdentityHashMapDemo{

	public static void main(String[] args){

		IdentityHashMap ihm = new IdentityHashMap();

		ihm.put(new Integer(10),"Vishal");
		ihm.put(new Integer(10),"Omkar");
		ihm.put(new Integer(10),"Shubham");

		System.out.println(ihm);
	}
}

//{10=Vishal, 10=Omkar, 10=Shubham}
