import java.util.*;

class Demo{

        String str = null;

        Demo(String str){
                this.str = str;
        }

        public String toString(){

                return str;
        }

        public void finalize(){

                System.out.println("Notify...");
        }
}

class GCDemo{

        public static void main(String[] args){

                Demo obj1 = new Demo("Vishal");
                Demo obj2 = new Demo("Shubham");
                Demo obj3 = new Demo("Omkar");

           	WeakHashMap hm = new WeakHashMap();

		hm.put(obj1,2002);
		hm.put(obj2,2001);
		hm.put(obj3,2003);

		obj1 = null;
		obj2 = null;

		System.gc();

		System.out.println(hm);
	}
}

/*
  
   Notify...
   {Omkar=2003}
   Notify...

 */

