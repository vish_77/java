import java.util.*;

class HashMapDemo{

	public static void main(String[] args){

		HashSet hs = new HashSet();
		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahul");
		hs.add("Badhe");
	
		System.out.println(hs);		//[Rahul, Ashish, Badhe, Kanha]
		
		HashMap hm = new HashMap();
		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");
		hm.put("Ashish","LTI");

		System.out.println(hm);		//{Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Infosys}

		//get()
		System.out.println(hm.get("Rahul"));	//BMC

		//keySet()
		System.out.println(hm.keySet());	//[Rahul, Ashish, Badhe, Kanha]

		//values()
		System.out.println(hm.values());	//[BMC, LTI, CarPro, Infosy]

		//entry()
		System.out.println(hm.entrySet());	//[Rahul=BMC, Ashish=LTI, Badhe=CarPro, Kanha=Infosys]

	}
}

