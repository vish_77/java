import java.io.*;
import java.util.*;

class PropertiesDemo{

	public static void main(String[] args) throws IOException{

		Properties obj = new Properties();

		FileInputStream fobj = new FileInputStream("friends.properties");
		obj.load(fobj);

		String name = obj.getProperty("Pankaj");
		System.out.println(name);

		obj.setProperty("Vishal","More");

		FileOutputStream outobj = new FileOutputStream("friends.properties");
		obj.store(outobj,"Updated by Vishal");
	}
}
