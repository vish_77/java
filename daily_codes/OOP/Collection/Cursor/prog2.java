
import java.util.*;

class CursorDemo{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(20);
		al.add(10.5f);
		al.add(10.5);
		al.add("Vishal");
		
		for(var x : al){
			System.out.println(x.getClass().getName());
		}

		/*
		java.lang.Integer
		java.lang.Float
		java.lang.Double
		java.lang.String
		*/

		//iterater

		ArrayList al2 = new ArrayList();
		al2.add("Vishal");
		al2.add("Pankaj");
		al2.add("Saurabh");
		al2.add("Raj");
		
		Iterator cursor = al2.iterator();

		System.out.println(cursor.getClass().getName());		//java.util.ArrayList$Itr

		while(cursor.hasNext()){

			if("Raj".equals(cursor.next())){
				cursor.remove();
			}
		}

		System.out.println(al2);					//[Vishal, Pankaj, Saurabh]

		//listIterator

		ListIterator litr = al2.listIterator();
		
		System.out.println(litr.getClass().getName());			//java.util.ArrayList$ListItr
		
		while(litr.hasNext()){
			
			System.out.println(litr.next());
		}

		while(litr.hasPrevious()){

			System.out.println(litr.previous());
		}
		
		/*
		Vishal
		Pankaj
		Saurabh
		Saurabh
		Pankaj
		Vishal
		*/


	}
}
