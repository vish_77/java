
import java.util.*;

class IteratatorDemo{

	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(30);
		al.add(40);
		al.add("Vishal");
		al.add("Saurabh");
		al.add("Pankaj");

		Iterator itr = al.iterator();

		while(itr.hasNext()){

			if("Saurabh".equals(itr.next())){
				itr.remove();
			}
		}
		System.out.println(al);
	}
}
