

import java.util.*;

class EnnumDemo{

	public static void main(String[] args){

		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		Enumeration cursor = v.elements();

		System.out.println(cursor.getClass().getName());	//java.util.Vector$1

		while(cursor.hasMoreElements()){

			System.out.println(cursor.nextElement());
		}
	}
}

		
