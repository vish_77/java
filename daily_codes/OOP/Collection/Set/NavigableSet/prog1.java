
/*
  public abstract E lower(E);
  public abstract E floor(E);
  public abstract E ceiling(E);
  public abstract E higher(E);
  public abstract E pollFirst();
  public abstract E pollLast();
*/


import java.util.*;

class NavigableSetDemo{

	public static void main(String[] args){

		NavigableSet ns = new TreeSet();

		ns.add(1);
		ns.add(3);
		ns.add(11);
		ns.add(5);
		ns.add(6);
		ns.add(16);
		ns.add(8);
		ns.add(9);
		ns.add(2);
		ns.add(20);

		System.out.println(ns);
		
		System.out.println(ns.lower(5));	//3
		
		System.out.println(ns.higher(20));	//null
		System.out.println(ns.higher(16));	//20

		System.out.println(ns.floor(7));	//6
		System.out.println(ns.floor(6));	//6

		System.out.println(ns.ceiling(8));	//8
		System.out.println(ns.ceiling(7));	//8

		System.out.println(ns.pollFirst());	//1
		System.out.println(ns.pollLast());	//20
	}	
}
