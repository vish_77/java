
import java.util.*;

class SortedSetDemo{

	public static void main(String[] args){

		SortedSet ss = new TreeSet();
		
		ss.add("Vishal");
		ss.add("Shubham");
		ss.add("Saurabh");
		ss.add("Pankaj");
		ss.add("Vikas");

		System.out.println(ss);

		System.out.println(ss.headSet("Saurabh"));
		System.out.println(ss.tailSet("Saurabh"));
		System.out.println(ss.subSet("Saurabh","Vishal"));
		System.out.println(ss.first());
		System.out.println(ss.last());
	}
}
