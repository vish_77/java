import java.util.*;

class Myclass implements Comparable{

	String str = null;

	Myclass(String str){

		this.str = str;

	}

	public int compareTo(Object obj){

		return 1;
	}

	public String toString(){

		return str;
	}
}

class TreeSetDemo{

	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add(new Myclass("Vishal"));
                ts.add(new Myclass("Shubham"));
                ts.add(new Myclass("Pankaj"));
                ts.add(new Myclass("Saurabh"));
                ts.add(new Myclass("Shubham"));

		System.out.println(ts);			//[Vishal, Shubham, Pankaj, Saurabh, Shubham]
	}
}
