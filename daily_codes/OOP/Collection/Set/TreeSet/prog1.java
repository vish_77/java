
//sort the values

import java.util.*;

class TreeSetDemo{

	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add("Vishal");
		ts.add("Shubham");
		ts.add("Pankaj");
		ts.add("Saurabh");
		ts.add("Shubham");

//		ts.add(10);			//ClassCastException: class java.lang.String cannot be cast to class java.lang.Integer
	
		System.out.println(ts);		//[Pankaj, Saurabh, Shubham, Vishal]
	}
}
