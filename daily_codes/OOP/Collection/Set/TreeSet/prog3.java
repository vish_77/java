
import java.util.*;

class Movies implements Comparable{

	String MovieName = null;
	float totalCall = 0.0f;

	Movies(String MovieName, float totalCall){

		this.MovieName = MovieName;
		this.totalCall = totalCall;

	}

	public int compareTo(Object obj){

//		return MovieName.compareTo(((Movies)obj).MovieName);		//[Gadar2, Jailer, OMG2]
		return -(MovieName.compareTo(((Movies)obj).MovieName));		//[OMG2, Jailer, Gadar2]	

	}

	public String toString(){

		return MovieName;
	}
}

class TreeSetDemo{

	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add(new Movies("Gadar2",300.00f));
		ts.add(new Movies("OMG2",200.00f));
		ts.add(new Movies("Jailer",250.00f));
		ts.add(new Movies("OMG2",200.00f));
		
		System.out.println(ts);
	}
}
