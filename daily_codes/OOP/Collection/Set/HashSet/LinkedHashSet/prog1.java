//insertion order is preserved

import java.util.*;

class LinkedHashSetDemo{

	public static void main(String[] args){

		LinkedHashSet lhs = new LinkedHashSet();
		
		lhs.add("Vishal");
		lhs.add("Shubham");
		lhs.add("Akash");
		lhs.add("Vikas");
		lhs.add("Pankaj");
		
		System.out.println(lhs);	//[Vishal, Shubham, Akash, Vikas, Pankaj]

	}
}
	
