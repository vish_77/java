import java.util.*;

class CricPlayer{

	int jerNo = 0;
	String name = null;

	CricPlayer(int jerNo, String name){

		this.jerNo = jerNo;
		this.name = name;
	}

	public String toString(){

		return name+" : " +jerNo;
	}
}

class HashDemo{

	public static void main(String[] args){

		LinkedHashSet lhs = new LinkedHashSet();

		lhs.add(new CricPlayer(18,"Virat"));
		lhs.add(new CricPlayer(7,"Dhoni"));
		lhs.add(new CricPlayer(45,"Rohit"));
		lhs.add(new CricPlayer(7,"Dhoni"));

		System.out.println(lhs);		//[Virat : 18, Dhoni : 7, Rohit : 45, Dhoni : 7]

	}
}
