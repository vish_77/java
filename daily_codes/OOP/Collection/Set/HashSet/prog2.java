import java.util.*;

class HashDemo{

	public static void main(String[] args){

		HashSet hs = new HashSet();

		hs.add(10);
		hs.add(20);
		hs.add(new Integer(10));	//warning: [removal] Integer(int) in Integer has been deprecated and marked for removal
		hs.add(new Integer(20));	//warning: [removal] Integer(int) in Integer has been deprecated and marked for removal

		System.out.println(hs);		//[20, 10]
	}
}
	
