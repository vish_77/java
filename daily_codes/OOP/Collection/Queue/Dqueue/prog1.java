
import java.util.*;

class DequeDemo{

	public static void main(String[] args){

		Deque obj = new ArrayDeque();

		obj.offer(10);
		obj.offer(20);
		obj.offer(30);
		obj.offer(50);
		obj.offer(40);

		System.out.println(obj);			//[10, 20, 30, 50, 40]

		obj.offerFirst(5);
		obj.offerLast(55);
		
		System.out.println(obj);			//[5, 10, 20, 30, 50, 40, 55]

		System.out.println(obj.pollFirst());		//5
		System.out.println(obj.pollLast());		//55

		System.out.println(obj);			//[10, 20, 30, 50, 40]

		System.out.println(obj.peekFirst());		//10
		System.out.println(obj.peekLast());		//40

		System.out.println(obj);			//[10, 20, 30, 50, 40]
		
		Iterator itr = obj.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}

		Iterator itr2 = obj.descendingIterator();
		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}

	}
}
