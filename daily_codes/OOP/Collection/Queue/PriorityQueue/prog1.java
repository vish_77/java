import java.util.*;

class PQueueDemo{

	public static void main(String[] args){

		PriorityQueue pq = new PriorityQueue();

		pq.offer(20);
		pq.offer(10);
		pq.offer(50);
		pq.offer(30);
		pq.offer(40);

		System.out.println(pq);
		
		PriorityQueue pq2 = new PriorityQueue();
		pq2.offer("Kanha");
		pq2.offer("Vishal");
		pq2.offer("Pankaj");
		pq2.offer("Raj");
		pq2.offer("Saurabh");

		System.out.println(pq2);
	
	}
}
		
