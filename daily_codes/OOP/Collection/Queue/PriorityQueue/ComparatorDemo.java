import java.util.*;

class Project{

	String projname;
	int teamsize;
	int duration;

	Project(String projname, int teamsize, int duration){

		this.projname = projname;
		this.teamsize = teamsize;
		this.duration = duration;

	}

	public String toString(){

		return projname + ":"+teamsize+":"+duration;
	}

}

class SortByName implements Comparator{

	public int compare(Object obj1, Object obj2){

		return ((Project)obj1).projname.compareTo(((Project)obj2).projname);
	}
}

class PriorityQueueDemo{

	public static void main(String[] args){

		PriorityQueue pq = new PriorityQueue(new SortByName());
		pq.offer(new Project("xyz",10,90));
		pq.offer(new Project("abc",20,45));
		pq.offer(new Project("pqr",50,30));

		System.out.println(pq);
	}

}
