import java.util.*;

class Project implements Comparable{

	String projname;
	int teamsize;
	int duration;

	Project(String projname, int teamsize, int duration){

		this.projname = projname;
		this.teamsize = teamsize;
		this.duration = duration;

	}

	public String toString(){

		return projname + ":"+teamsize+":"+duration;
	}

	public int compareTo(Object obj){
		
		return projname.compareTo(((Project)obj).projname);
	}


}

class PriorityQueueDemo{

	public static void main(String[] args){

		PriorityQueue pq = new PriorityQueue();
		pq.offer(new Project("xyz",10,90));
		pq.offer(new Project("abc",20,45));
		pq.offer(new Project("pqr",50,30));

		System.out.println(pq);
	}

}
