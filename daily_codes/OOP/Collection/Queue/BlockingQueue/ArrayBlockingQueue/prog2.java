import java.util.concurrent.*;
import java.util.*;

class BlockingQueueDemo{

	public static void main(String[] args)throws InterruptedException{

		BlockingQueue bQueue = new ArrayBlockingQueue(5);

		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);
		bQueue.offer(50);
		bQueue.offer(60);

		System.out.println(bQueue);

		bQueue.offer(40,5,TimeUnit.SECONDS);
		
		System.out.println(bQueue);

		bQueue.take();
		System.out.println(bQueue);

		ArrayList al = new ArrayList();
		System.out.println(al);

//		bQueue.drainTo(al);		to move all 
		bQueue.drainTo(al,3);

		System.out.println(al);
		System.out.println(bQueue);
	}
}

