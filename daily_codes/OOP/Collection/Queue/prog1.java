import java.util.*;

class QueueDemo{

	public static void main(String[] args){

		Queue que = new LinkedList();

		que.offer(10);
		que.offer(20);
		que.offer(50);
		que.offer(30);
		que.offer(40);

		System.out.println(que);

		que.poll();
		que.remove();

		System.out.println(que);

		System.out.println(que.peek());
		System.out.println(que.element());

		System.out.println(que);		
		
		Queue que2 = new LinkedList();
//		que2.remove();				//Exception in thread "main" java.util.NoSuchElementException

//		System.out.println(que2.element());	//Exception in thread "main" java.util.NoSuchElementException


	}
}

