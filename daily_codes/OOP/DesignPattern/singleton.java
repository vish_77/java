class Singleton{

	static Singleton obj = new Singleton();

	private Singleton(){
		
		System.out.println("In construcotr");
	}

	static Singleton getObject(){

		return obj;
	}
}

class Client{

	public static void main(String[] args){

		Singleton obj1 = Singleton.getObject();
		System.out.println(obj1);
		
		Singleton obj2 = Singleton.getObject();
		System.out.println(obj2);
		
		Singleton obj3 = Singleton.getObject();
		System.out.println(obj3);
	}
}
		
/*
In construcotr
Singleton@6ff3c5b5
Singleton@6ff3c5b5
Singleton@6ff3c5b5
*/
		
