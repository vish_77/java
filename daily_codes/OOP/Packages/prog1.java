
import java.util.Scanner;
import arithfun.*;

class Client{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int y = sc.nextInt();

		Addition obj = new Addition(x,y);
		System.out.println("Addition is : "+obj.add());

		Multiplication obj1 = new Multiplication(x,y);
		System.out.println("Multiplication is : "+obj1.mult());

		Division obj2 = new Division(x,y);
		System.out.println("Division is :"+obj2.div());
		
	}	
}
