class Demo{

	void fun(Object obj){

		System.out.println("Object");
	}

	void fun(String str){

		System.out.println("String");
	}
}

class Client{

	public static void main(String[] args){

		Demo obj = new Demo();

		obj.fun("Vishal");			//String
		obj.fun(new StringBuffer("vishal"));	//Object
		obj.fun(null);				//String - both function matches but more priority is given to child
	}
}
