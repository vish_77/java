class Demo{

	void fun(String str){				//String str = "Vishal";

		System.out.println("String");
	}

	void fun(StringBuffer str1){			//StringBuffer str1 = new StringBuffer("Vishal");

		System.out.println("StringBuffer");
	}

}

class Client{

	public static void main(String[] args){

		Demo obj = new Demo();
		obj.fun("Vishal");			//String

		obj.fun(new StringBuffer("Vishal"));	//StringBuffer
		
//		obj.fun(null);				//error: reference to fun is ambiguous
	}
}
