class Demo{

	void fun(int x){

		System.out.println(x);
	}

	void fun(float y){

		System.out.println(y);
	}

	void fun(Demo obj){

		System.out.println("In demo para");
		System.out.println(obj);
	}

	public static void main(String[] args){

		Demo obj = new Demo();

		obj.fun(10);
		obj.fun(10.34f);
		obj.fun(obj);
	}
}

/*
10
10.34
In demo para
Demo@7de26db8
*/
