class Parent{

	Parent(){
		
		System.out.println("Parent Constructor");
	}

	void Property(){

		System.out.println("Home,Car,Gold");

	}

	void Marry(){

		System.out.println("Deepika Padukone");
	}

}

class Child extends Parent{

	Child(){

		System.out.println("Child constructor");
	}

	void Marry(){

		System.out.println("Rashmika Mandana");
	}
}

class Client{

	public static void main(String[] args){

		Child obj = new Child();
		obj.Property();
		obj.Marry();
	}
}
/*
Parent Constructor
Child constructor
Home,Car,Gold
Rashmika Mandana
*/
