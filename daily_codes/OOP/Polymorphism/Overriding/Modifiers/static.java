
class Parent{

	static void fun(){
		
		System.out.println("In parent fun");
	}

	void gun(){

		System.out.println("In parent gun");
	}
}

class Child extends Parent{

	void fun(){					//error: fun() in Child cannot override fun() in Parent
							//overridden method is static
		System.out.println("In child fun");
	}

	static void gun(){				//error: gun() in Child cannot override gun() in Parent
							//overriding method is static
		System.out.println("In child gun");
	}
}
