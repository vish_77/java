class Parent{

	void fun(){

		System.out.println("Parent fun");
	}
}

class Child1 extends Parent{

	final void fun(){

		System.out.println("Child1 fun");
	}
}

class Child2 extends Child1{
	
	void fun(){					//error: fun() in Child2 cannot override fun() in Child1
							//overridden method is final
		System.out.println("Child2 fun");
	}
}
