class Parent{

	public void fun(){

		System.out.println("Parent fun");
	}
}

class child extends Parent{

	void fun(){

		System.out.println("Child fun");
	}
}

//error : attempting to assign weaker access privileges; was public
