class Parent{

	static void fun(){

		System.out.println("In parent fun");
	}

}
class Child extends Parent{

	static void fun(){

		System.out.println("In child fun");
	}
}

class Client{

	public static void main(String[] args){

		Parent obj1 = new Parent();
		obj1.fun();			//In parent fun

		Parent obj2 = new Child();	
		obj2.fun();			//In parent fun - jaych reference ahe tyla call jato

		Child obj3 = new Child();	
		obj3.fun();			//In Child fun

	}
}
