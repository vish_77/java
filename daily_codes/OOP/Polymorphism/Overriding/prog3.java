class Parent{

	void fun(int x){

		System.out.println("In parent fun");
	}

}
class Child extends Parent{

	void fun(){

		System.out.println("In child fun");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj = new Child();
//		obj.fun();		//error: method fun in class Parent cannot be applied to given types;

		obj.fun(10);
	}
}
