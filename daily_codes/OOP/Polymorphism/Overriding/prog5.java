
//Covarrient return types

class Parent{

	Object fun(){

		System.out.println("Parent fun");
		return new Object();
	}

/*	String gun(){

		return "Vishal";
	}
*/
}
class Child extends Parent{

	String fun(){

		System.out.println("Child fun");
		return "Vishal";
	}

/*	Object gun(){				//error: gun() in Child cannot override gun() in Parent
						//return type Object is not compatible with String	
		return new Object();
	}
*/
}

class Client {

	public static void main(String[] args){

		Parent p = new Child();
		p.fun();			//Child fun
//		p.gun();
	}
}
