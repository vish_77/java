class Parent{

	private void fun(){

		System.out.println("In parent fun");
	}
}

class Child extends Parent{

	void fun(){

		System.out.println("In child fun");
	}
}

class Client{

	static public void main(String[] args){

		Parent p = new Child();
		p.fun();		//error - fun() has private access in Parent - both classes have individual fun()
		
	}
}
