class Parent{

	Parent(){

		System.out.println("In parent constructor");
	}

	void fun(){

		System.out.println("In parent fun");
	}
}

class Child extends Parent{

	Child(){

		System.out.println("In child constructor");
	}

	void fun(){

		System.out.println("In child fun");
	}

	void gun(){

		System.out.println("In gun");

	}

}

class Client{

	public static void main(String[] args){


		Child obj1 = new Child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
//		obj2.gun();		//cannot find symbol-child chy gosti parent la disat nahi

		Parent obj3 = new Child();	//child cha object create hoto
		obj3.fun();
//		obj3.gun();		//error: cannot find symbol-compile time la gun() parent madhe check hote.
					//run time la child chi method execute hote hence donhi class madhe method available pahije
					
	}
}
