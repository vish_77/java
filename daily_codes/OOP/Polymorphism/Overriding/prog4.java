class Parent{

	char fun(){

		System.out.println("Parent fun");
		return 'V';
	}
}
class Child extends Parent{

	int fun(){

		System.out.println("Child fun");
		return 10;
	}
}

class Client {

	public static void main(String[] args){

		Parent p = new Child();
		p.fun();
	}
}

//error: fun() in Child cannot override fun() in Parent
//return type int is not compatible with char
