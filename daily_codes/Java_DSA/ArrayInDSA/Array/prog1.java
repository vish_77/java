/*

   count no of elements having atleast 1 element greater than itself

*/

import java.util.*;

class Demo{
static int count(int arr[]){

	int max = Integer.MIN_VALUE;

	for(int i = 0; i<arr.length; i++){

		if(arr[i]>max){

			max = arr[i];
		}
	}

	int total = 0;
	for(int i = 0; i<arr.length; i++){

		if(arr[i] < max)
			total ++;
	}
	return total;
}

	public static void main(String[] args){

		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};

		System.out.println("No of elements having atleast 1 element greater than itself : "+count(arr));
	}
}
