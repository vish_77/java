/*
return the count of pairs (i,j) with Arr[i] + Arr[j] = k.
*/

class Demo{

	static int Count(int arr[], int N, int K){
		
		int count = 0;
		for(int i =0; i<arr.length; i++){

			for(int j = i+1; j<arr.length; j++){

				if(arr[i] + arr[j] == K)
					count += 2;
				
			}
		}

		return count;
	}


	public static void main(String[] args){

		int arr[] = new int[]{3,5,2,1,-3,7,8,5,6,13};
		int k = 10;
		int N = 10;

		System.out.println("Count of pairs:"+Count(arr,N,k));
	}
}
