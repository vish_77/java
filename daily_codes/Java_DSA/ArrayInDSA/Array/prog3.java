/*
return count of pair(i,j) such that
a. i<j
b. arr[i] = 'a'
c. arr[j] = 'g'

arr. [a,b,c,g,a,g]
op = 3

*/

class Demo{


	static int PairCnt(char [] arr){

		int acount = 0;
		int count = 0;

		for(int i = 0; i<arr.length; i++){

			if(arr[i] == 'a')
				acount ++;

			if(arr[i] == 'g')
				count = count + acount;
		}

		return count;
	}

	public static void main(String[] args){

		char arr[] = {'a','b','c','g','a','g'};

		int count = PairCnt(arr);

		System.out.println(count);
	}
}
