/*
Equilibrium Index of array

you are give array A of integer of size N, find equilibrium index .
the Equilibrium index of an array is an index such that the sum of elements
of both side of that index is same.

A=[-7,1,5,2,-4,3,0]
op = 3

*/

import java.util.*;
class Demo{

	//Bruteforce approach.
	static int Equilibrium1(int arr[]){

		for(int i = 0; i<arr.length; i++){

			int sum1 = 0;
			for(int j = 0; j<i; j++){

				sum1 = sum1 + arr[j];
			}

			int sum2 = 0;
			for(int k = i+1; k<arr.length; k++){
				
				sum2 = sum2 + arr[k];
			}

			if(sum1 == sum2)
				return i;
		}

		return -1;
	}

	//Using Prefix sum.
	static int Equilibrium2(int arr[]){

		int prefix[] = new int[arr.length];
		prefix[0] = arr[0];

		for(int i = 1; i<arr.length; i++){

			prefix[i] = prefix[i-1] + arr[i];
		}

		for(int i = 0; i<arr.length; i++){

			if(i==0){
				int sum = prefix[arr.length-1] - prefix[i];
				if(sum == 0)
					return i;
			}else{
				int sum1 = prefix[i-1];
				int sum2 = prefix[arr.length-1] - prefix[i];

				if(sum1 == sum2)
					return i;
			}
		}
		return -1;

	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		System.out.println("Enter the elements:");
		for(int i = 0 ;i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		System.out.println("Equilibrium index : "+Equilibrium1(arr));
		System.out.println("Equilibrium index : "+Equilibrium2(arr));
	}
}


