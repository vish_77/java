import java.util.*;
class Node{

	int data;
	Node next;

	Node (int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addNode(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null)
				temp = temp.next;

			temp.next = newNode;
		}
	}

	int mid1(){

		int mid = countNode()/2;
		Node temp = head;

		while(mid != 0){
			
			temp = temp.next;
			mid--;
		}

		return temp.data;
	}

	int mid2(){

		Node ptr1 = head;
		Node ptr2 = head.next;

		while(ptr2 != null){

			ptr2 = ptr2.next;
			if(ptr2 != null)
				ptr2 = ptr2.next;

			ptr1 = ptr1.next;
		}

		return ptr1.data;
	}

	int countNode(){

		int count = 0;
		Node temp = head;

		while(temp != null){

			count++;
			temp = temp.next;
		}

		return count;
	}
	
	void printLL(){
		
		if(head == null){
			System.out.println("LL is empty");
			return;
		}

		Node temp = head;

		while(temp.next != null){

			System.out.print(temp.data +" -> ");
			temp= temp.next;
		}
			
		System.out.println(temp.data);
	}
}


class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char ch ;

		do{
			System.out.println("Singly Linkedlist");
			System.out.println("1. addNode");
			System.out.println("2. printLL");
			System.out.println("3. mid using counter");
			System.out.println("4. mid using two pointer");
			System.out.println("5. countNode");
			
			System.out.println("Enter your choice :");
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();

			switch(choice){

				case 1:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       ll.addNode(data);
					}
					break;

				case 3:
					System.out.println(ll.mid1());	
					break;

				case 4:
					System.out.println(ll.mid2());	
					break;

				case 2:
					ll.printLL();
					break;

				case 5:
					System.out.println(ll.countNode());
					break;				

				default:
					System.out.println("Invalid choice");
			}

			System.out.println("Coutinue...?");
			ch = sc.next().charAt(0);


		}while(ch == 'Y' || ch == 'y');


	}
}


