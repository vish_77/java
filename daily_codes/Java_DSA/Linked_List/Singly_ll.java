import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){
		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addFirst(int data){

		Node newNode = new Node(data);

		if(head == null)
			head = newNode;
		else{
			newNode.next = head;
			head = newNode;
		}
	}

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null)
			head = newNode;
		
		else{
			Node temp = head;
			
			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}	

	void addAtPos(int pos,int data){

		Node newNode = new Node(data);
		
		if(pos < 1 || pos > countNode()+1){
			System.out.println("Invalid position");
			return;
		}

		if(pos == 1)
			addFirst(data);

		else if(pos == countNode()+1)

			addLast(data);
		else{
			Node temp = head;

			while(pos-2 != 0){

				temp = temp.next;
				pos --;
			}
			newNode.next = temp.next;
			temp.next = newNode;
		}
	}

	void deleteFirst(){

		if(head == null){

			System.out.println("LinkedList is empty");
		}else if(head.next == null){

			head = null;
		}else{
			head = head.next;
		}
	}

	void deleteLast(){

		if(head == null){

			System.out.println("LinkedList is empty");

		}else if(head.next == null){

			head = null;
		}else{

			Node temp = head;

			while(temp.next.next != null){

				temp = temp.next;
			}

			temp.next = null;
		}
	}

	void deleteAtPos(int pos){

		if(pos < 1 || pos > countNode()){
                        System.out.println("Invalid position");
                        return;
                }

                if(pos == 1)
                        deleteFirst();

                else if(pos == countNode())

                        deleteLast();
                else{
                        Node temp = head;

                        while(pos-2 != 0){

                                temp = temp.next;
                                pos --;
                        }
                
                        temp.next = temp.next.next;
                }
	}



	int countNode(){

		Node temp = head;
		int count = 0;

		while(temp != null){

			count ++;
			temp = temp.next;
		}
		return count;
	}


	void printLL(){

		if(head == null){
			System.out.println("Linked list is empty");
		}else{
			Node temp = head;
			while(temp.next != null){
				
				System.out.print(temp.data +" -> ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}


}


class Client{
	
	public static void main(String [] args){

		LinkedList ll = new LinkedList();

		char ch = 'a';

		do{
			System.out.println("Singly Linkedlist");
			System.out.println("1. addFirst");
			System.out.println("2. addLast");
			System.out.println("3. addAtPos");
			System.out.println("4. deleteFirst");
			System.out.println("5. deleteLast");
			System.out.println("6. deleteAtPos");
			System.out.println("7. printLL");
			System.out.println("8. countNode");

			System.out.println("Enter your choice :");
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();

			switch(choice){

				case 1:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       ll.addFirst(data);
					}
					break;

				case 2:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       ll.addLast(data);
					}
					break;

				case 3:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       System.out.println("Enter pos");
					       int pos = sc.nextInt();
					       ll.addAtPos(pos,data);
					}
					break;

				case 4:{
					       
					       ll.deleteFirst();
					}
					break;

				case 5:{
					       ll.deleteLast();
					}
					break;

				case 6:{
					       System.out.println("Enter Pos");
					       int pos = sc.nextInt();
					       ll.deleteAtPos(pos);
					}
					break;

				case 7:{
					       ll.printLL();
					}
					break;

				case 8:{
					       
					       System.out.println(ll.countNode());
					}
					break;

				default:
					System.out.println("Invalid choice");
			}

			System.out.println("Coutinue...?");
			ch = sc.next().charAt(0);


		}while(ch == 'Y' || ch == 'y');
	}
	
}
