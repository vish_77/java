
import java.util.*;

class Node{

	Node prev = null;
	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class DoublyLL{

	Node head = null;

	void addFirst(int data){
		
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{

			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	}

	void addLast(int data){

		Node newNode = new Node(data);
		
		if(head == null)
			head = newNode;
		else{
			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			newNode.prev = temp;
			temp.next = newNode;
		}
	}

	void addAtPos(int data,int pos){

		if(pos < 1 || pos > nodeCount()+1){
			System.out.println("Invalid position");
			return;
		}

		if(pos == 1)
			addFirst(data);

		else if(pos == nodeCount()+1)
			addLast(data);

		else{

			Node newNode = new Node(data);

			Node temp = head;

			while(pos-2 != 0){
				
				temp = temp.next;
				pos --;
			}
			newNode.next = temp.next;
			temp.next.prev = newNode;
			temp.next = newNode;
			newNode.prev = temp;
		}
	}

	void deleteFirst(){

		if(head == null)
			System.out.println("LL is empty");
		
		else if(head.next == null)
			head = null;

		else{
			head = head.next;
			head.prev = null;
		}
	}

	void deleteLast(){

		if(head == null)
			System.out.println("LL is empty");

		else if(head.next == null)

			head = null;
		else{
			Node temp = head;

			while(temp.next.next != null)
				temp = temp.next;

			temp.next = null;
		}
	}
	
	void deleteAtPos(int pos){

		if(pos < 1 || pos > nodeCount()){

			System.out.println("Invalid position");
			return;
		}

		if(pos == 1)
			deleteFirst();
		else if(pos == nodeCount())
			deleteLast();

		else{
			Node temp = head;

			while(pos - 2 != 0){

				temp = temp.next;
				pos --;
			}

			temp.next = temp.next.next;
			temp.next.prev = temp;
		}
	}

	int nodeCount(){

		int count = 0;
		Node temp = head;

		while(temp != null){

			count ++;
			temp = temp.next;
		}

		return count;
	}

	void printLL(){

		if(head == null){
		
			System.out.println("LL is empty");
			return;	
		}
	
		Node temp = head;

		while(temp.next != null){

			System.out.print(temp.data +" <-> ");
			temp = temp.next;
		}
		System.out.println(temp.data);
	}
}

class Client{

	public static void main(String[] args){

		DoublyLL dll = new DoublyLL();
		char ch = ' ';

		do{
			System.out.println("Doubly Linkedlist");
			System.out.println("1. addFirst");
			System.out.println("2. addLast");
			System.out.println("3. addAtPos");
			System.out.println("4. deleteFirst");
			System.out.println("5. deleteLast");
			System.out.println("6. deleteAtPos");
			System.out.println("7. nodeCount");
			System.out.println("8. printLL");

			System.out.println("Enter your choice :");
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();

			switch(choice){

				case 1:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       dll.addFirst(data);
					}
					break;

				case 2:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       dll.addLast(data);
					}
					break;

				case 3:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       System.out.println("Enter pos");
					       int pos = sc.nextInt();
					       dll.addAtPos(data,pos);
					}
					break;

				case 4:{
					       
					       dll.deleteFirst();
					}
					break;

				case 5:{
					       dll.deleteLast();
					}
					break;

				case 6:{
					       System.out.println("Enter Pos");
					       int pos = sc.nextInt();
					       dll.deleteAtPos(pos);
					}
					break;

				case 7:{
					       System.out.println(dll.nodeCount());
					}
					break;

				case 8:{
					       
					       dll.printLL();
					}
					break;

				default:
					System.out.println("Invalid choice");
			}

			System.out.println("Coutinue...?");
			ch = sc.next().charAt(0);


		}while(ch == 'Y' || ch == 'y');

	}
}
