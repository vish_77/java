import java.util.*;
class Node{

	int data;
	Node next;

	Node (int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addNode(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null)
				temp = temp.next;

			temp.next = newNode;
		}
	}

	void inplaceRev(){

		Node Prev = null;
		Node Curr = head;
		Node Next = null;

		while(Curr != null){

			Next = Curr.next;
			Curr.next = Prev;
			Prev = Curr;
			Curr = Next;
		}

		head = Prev;
	}

	int countNode(){

		int count = 0;
		Node temp = head;

		while(temp != null){

			count++;
			temp = temp.next;
		}

		return count;
	}
	
	void printLL(){
		
		if(head == null){
			System.out.println("LL is empty");
			return;
		}

		Node temp = head;

		while(temp.next != null){

			System.out.print(temp.data +" -> ");
			temp= temp.next;
		}
			
		System.out.println(temp.data);
	}
}


class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char ch ;

		do{
			System.out.println("Singly Linkedlist");
			System.out.println("1. addNode");
			System.out.println("2. printLL");
			System.out.println("3. implace Rev");
			System.out.println("4. countNode");
			
			System.out.println("Enter your choice :");
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();

			switch(choice){

				case 1:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       ll.addNode(data);
					}
					break;

				case 3:
				        ll.inplaceRev();					
					break;

				case 2:
					ll.printLL();
					break;

				case 4:
					System.out.println(ll.countNode());
					break;				

				default:
					System.out.println("Invalid choice");
			}

			System.out.println("Coutinue...?");
			ch = sc.next().charAt(0);


		}while(ch == 'Y' || ch == 'y');


	}
}


