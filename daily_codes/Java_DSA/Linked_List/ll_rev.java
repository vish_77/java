import java.util.*;
class Node{

	int data;
	Node next;

	Node (int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addNode(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null)
				temp = temp.next;

			temp.next = newNode;
		}
	}

	void revLL(){

		Node temp1 = head;

		int itr = countNode()/2;
		int itr2 = countNode();

		while(itr != 0){
			
			Node temp2 = head;
			int x = itr2;

			while(itr2 - 1 != 0){
				temp2 = temp2.next;
				itr2--;
			}

			int temp = temp1.data;
			temp1.data = temp2.data;
			temp2.data = temp;

			temp1 = temp1.next;
			itr2 = x-1;
			itr--;
		}
	}

	int countNode(){

		int count = 0;
		Node temp = head;

		while(temp != null){

			count++;
			temp = temp.next;
		}

		return count;
	}
	
	void printLL(){

		Node temp = head;

		while(temp.next != null){

			System.out.print(temp.data +" -> ");
			temp= temp.next;
		}
			
		System.out.println(temp.data);
	}
}


class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char ch ;

		do{
			System.out.println("Singly Linkedlist");
			System.out.println("1. addNode");
			System.out.println("2. printLL");
			System.out.println("3. revLL");
			
			System.out.println("Enter your choice :");
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();

			switch(choice){

				case 1:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       ll.addNode(data);
					}
					break;

				case 3:
				        ll.revLL();
					
					break;

				case 2:
					ll.printLL();
					break;

				case 4:
					System.out.println(ll.countNode());
					break;				

				default:
					System.out.println("Invalid choice");
			}

			System.out.println("Coutinue...?");
			ch = sc.next().charAt(0);


		}while(ch == 'Y' || ch == 'y');


	}
}


