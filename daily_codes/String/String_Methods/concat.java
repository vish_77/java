class ConcatDemo{

	public static void main(String[] args){

		String str1 = "vishal";
		String str2 = "more";

		String str3 = str1.concat(str2);
		System.out.println(str3);		//vishalmore
	}
}
