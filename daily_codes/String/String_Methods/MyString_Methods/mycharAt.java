
import java.io.*;

class Demo{

	static char MycharAt(String str, int index){

		char carr[] = str.toCharArray();
		return carr[index];

	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter a string = ");
		String str = br.readLine();
	
		System.out.println("Enter an integer");
		int index = 0;
	
		try{
			index = Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj){
			System.out.println("Enter an integer");
			index = Integer.parseInt(br.readLine());
		}			

		System.out.println("The char at "+index+" is = "+MycharAt(str,index));
		
	}
}
