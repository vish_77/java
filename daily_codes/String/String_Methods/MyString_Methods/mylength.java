
import java.io.*;

class Demo{

	static int Mylength(String str){

		char carr[] = str.toCharArray();
		int count = 0;

		for(int i =0; i<carr.length; i++){
			
			count ++;
		}
		return count;

	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a string = ");
		String str = br.readLine();

		System.out.println("The length of string = "+Mylength(str));
	}
}
