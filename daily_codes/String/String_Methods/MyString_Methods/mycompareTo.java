
import java.io.*;

class Demo{

	static int MycompareTo(String str1,String str2){

		char carr1[] = str1.toCharArray();
		char carr2[] = str2.toCharArray();
	
		if(carr1.length != carr2.length)
			return carr1.length - carr2.length;

		for(int i = 0; i< carr1.length && i<carr2.length; i++){
			
			if(carr1[i] != carr2[i])
				return carr1[i] - carr2[i];
					
		}

		return 0;

	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string1 = ");
		String str1 = br.readLine();
		
		System.out.println("Enter string2 = ");
		String str2 = br.readLine();

		System.out.println(MycompareTo(str1,str2));
	}
}
