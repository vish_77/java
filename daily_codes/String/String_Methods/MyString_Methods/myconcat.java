
import java.io.*;

class Demo{

	static String Myconcat(String str1,String str2){

		return str1 + str2;

	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string1 = ");
		String str1 = br.readLine();
		
		System.out.println("Enter string2 = ");
		String str2 = br.readLine();
	
		String str3 = Myconcat(str1,str2);
		System.out.println(str3);
	}
}
