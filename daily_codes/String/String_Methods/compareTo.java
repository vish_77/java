class Demo{

	public static void main(String[] args){

		String str1 = "Vishal";
		String str2 = "Vishal";
		String str3 = "VishAl";
		String str4 = "Iamvishal";

		System.out.println(str1.compareTo(str2));	//0
		System.out.println(str1.compareTo(str3));	//32
		System.out.println(str1.compareTo(str4));	//13
	}
}
