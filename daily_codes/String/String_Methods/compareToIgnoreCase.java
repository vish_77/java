class Demo{

	public static void main(String[] args){

		String str1 = "Vishal";
		String str3 = "vIshAl";
		String str4 = "vishalmoreisaking";

		System.out.println(str1.compareToIgnoreCase(str3));	//0
		System.out.println(str1.compareToIgnoreCase(str4));	//-11
	}
}
