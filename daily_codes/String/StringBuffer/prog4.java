class Demo{

	public static void main(String[] args){

		String str1 = "Shashi";
		String str2 = new String("Core2Web");
		StringBuffer str3 = new StringBuffer("Biencaps");
		StringBuffer str = new StringBuffer("abc");

//		String str4 = str1.append(str3);	//error: cannot find symbol
//		String str4 = str3.append(str1);	//error: incompatible types: StringBuffer cannot be converted to String

		StringBuffer str4 = str3.append(str1);
//		String str5 = str1.concat(str3);	//error: incompatible types: StringBuffer cannot be converted to String
	
		str.append("def");

		System.out.println(str1);	//Shashi
		System.out.println(str2);	//Core2Web
		System.out.println(str3);	//BiencapsShashi
		System.out.println(str4);	//BiencapsShashi
		System.out.println(str);	//abcdef
		
	}
}
