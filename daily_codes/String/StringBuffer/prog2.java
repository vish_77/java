class Demo{

	public static void main(String [] args){

		StringBuffer str1 = new StringBuffer();
		System.out.println(str1.capacity());		//16
		str1.append("abcdefghijklmnoqrs");
		System.out.println(str1.capacity());		//34

		StringBuffer str2 = new StringBuffer("Vishal");
		System.out.println(str2.capacity());		//22
		str2.append("More is a king");
		System.out.println(str2.capacity());		//22
		str2.append(".....");
		System.out.println(str2.capacity());		//46
	}
}
