class Demo{
	public static void main(String[] args){

		StringBuffer str1 = new StringBuffer("Vishal");
		System.out.println(System.identityHashCode(str1));	//0x1000

		str1.append(str1);
		System.out.println(str1);				//VishalVishal
		System.out.println(System.identityHashCode(str1));	//0x1000
	}
}
		

