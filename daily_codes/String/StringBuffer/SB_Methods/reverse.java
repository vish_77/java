class Demo{

	public static void main(String[] args){

		StringBuffer str = new StringBuffer("Vishal");
		System.out.println(str.reverse());	//lahsiV

		String str2 = "Vishal";
		StringBuffer str3 = new StringBuffer(str2);
		str2 = str3.reverse().toString();

		System.out.println(str2);		//lahsiV
	}
}
