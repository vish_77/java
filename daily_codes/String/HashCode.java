class Demo{

	public static void main(String [] args){

		String str1 = "Vishal";
		String str2 = new String("Vishal");
		String str3 = "Vishal";
		String str4 = new String("Vishal");

		System.out.println(str1.hashCode());	//1732361581
		System.out.println(str2.hashCode());	//1732361581
		System.out.println(str3.hashCode());	//1732361581
		System.out.println(str4.hashCode());	//1732361581
	}
}
