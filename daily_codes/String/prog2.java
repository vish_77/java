class StringDemo{
	
	public static void main(String[] args){

		String str1 = "Water";
		String str2 = str1;
		String str3 = new String(str2);

		System.out.println(System.identityHashCode(str1));	//1973538135
		System.out.println(System.identityHashCode(str2));	//1973538135
		System.out.println(System.identityHashCode(str3));	//1365202186

		String str4 = "Vishal";
		String str5 = "More";
		String str6 = "VishalMore";
		String str7 = str4 + str5;
		String str8 = str4.concat(str5);
		
		System.out.println(System.identityHashCode(str6));	//987405879
		System.out.println(System.identityHashCode(str7));	//1555845260
		System.out.println(System.identityHashCode(str8));	//874088044
	}
}
		

