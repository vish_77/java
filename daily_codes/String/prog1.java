class StringDemo{

	public static void main(String[] args){

		String str1 = "Vishal";
		String str2 = "Vishal";
		String str3 = new String("Vishal");
		String str4 = new String("Vishal");
		char str5[] = new char[]{'V','i','s','h','a','l'};

		System.out.println(str1);	//0x1000
		System.out.println(str2);	//0x1000
		System.out.println(str3);	//0x2000
		System.out.println(str4);	//0x3000
		System.out.println(str5);	//0x4000
		
		System.out.println(System.identityHashCode(str1));	//Vishal
		System.out.println(System.identityHashCode(str2));	//Vishal
		System.out.println(System.identityHashCode(str3));	//Vishal
		System.out.println(System.identityHashCode(str4));	//Vishal
		System.out.println(System.identityHashCode(str5));	//Vishal

	}
}
